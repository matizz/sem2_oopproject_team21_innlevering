import * as React from 'react';
import { Component } from 'react-simplified';
import ReactDOM from 'react-dom';
import { NavLink, Router, Route } from 'react-router-dom';
import Login from './View_Login/view_loginIndex';
import App from './View_App/view_appIndex';
import createHashHistory from 'history/createHashHistory';
import { loginService } from './Services_Login/Service_login';

const history = createHashHistory();

class InitialPage extends Component {

    render() {
        return (
            <div>
                {/* <NavLink to="/Login">Click here to log in</NavLink> */}
            </div>
        );
    }

    componentWillUnmount() {
        //register the time for the exit
        localStorage.setItem("app-last-exit", Date.now());
    }

    mounted() {
        let lastExit = localStorage.getItem("app-last-exit");

        loginService.VerifySession(lastExit);

        let loginStatus = localStorage.getItem("loginStatus");

        if (loginStatus === "true") {
            history.push('/App');
        }
        else {
            history.push('/Login')
        }
    }
}

/**
 * Main routes. 
 * Login route: The login page allows the user to login to the application. found in src/view_Login
 * App route: The application that allows the user to manage orders and bikes. found in src/view_App
 *
 */
ReactDOM.render(
    <Router history={history}>
        <div>
            <Route exact path="/" component={InitialPage} />
            <Route path="/Login" component={Login} />
            <Route path="/App" component={App} />
        </div>
    </Router>,
    document.getElementById('root')
);