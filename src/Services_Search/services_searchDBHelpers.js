import { connection } from './../Services_MYSQL/service_mysqlConnection';

class DbSearchHelpers {
    GetTypeTranslation(callBack) {
        connection.query('SELECT bike_typeID, bike_type FROM bike_type', (error, results) => {
            if (error) return console.error(error);

            callBack(results);
        })
    }

    GetAgeTranslation(callBack) {
        connection.query('SELECT user_age, age FROM user_age', (error, results) => {
            if (error) return console.error(error);

            callBack(results);
        })
    }
}

export let dbSearchHelpers = new DbSearchHelpers();