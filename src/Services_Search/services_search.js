import { searchDictionary } from './services_searchDictionary';
/**
 * Problems with this searching method. Valueas are always exact, meaning that you cannot choose if you want to search for
 * paritial matches in the values supplied with the keywords. I.e searching with the term "comment good" will not match with
 * all orders with comments that contain the word good. Only comments with comments equal to "good" is matched.
 */

/**
 * ==TODO==
 * Search in customers
 * 
 */

//====KEYWORD ALIASES====

let bikeId_Alias = ['id', 'bikeid'];
let chassienum_Alias = ['chassienum', 'rammenummer'];
let name_Alias = ['name', 'navn'];
let type_Alias = ['type'];
let make_Alias = ['make', 'merke'];
let model_Alias = ['model'];
let size_Alias = ['size', 'størrelse'];
let userLevel_Alias = ['userlevel', 'brukernivå', 'nivå'];
let userGender_Alias = ['usergender', 'kjønn', 'gender'];
let userAge_Alias = ['age', 'alder', 'userage'];
let address_Alias = ['adress', 'address', 'adresse'];
let currentAddress_Alias = ['currentaddress', 'currentlocation', 'cl'];
let status_Alias = ['status'];

let orderId_Alias = ['order_id', 'id'];
let fromDate_Alias = ['from_date', 'from', 'fra_dato', 'fra'];
let toDate_Alias = ['to_date', 'to', 'til_dato', 'til'];
let orderComment_Alias = ['order_comment', 'comment', 'comments', 'kommentarer', 'kommentar'];
let customerId_Alias = ['customer_id', 'customer', 'kundeid'];
let fulfilled_Alias = ['fulfilled', 'status'];
//====KEYWORDS====

//keywords
let bikeKeywords = {
    'bike_id': bikeId_Alias, 'chassienum': chassienum_Alias,
    'name': name_Alias, 'type': type_Alias, 'make': make_Alias,
    'model': model_Alias, 'size': size_Alias, 'user_lever': userLevel_Alias,
    'user_gender': userGender_Alias, 'user_age': userAge_Alias, 'adress': address_Alias,
    'current_adress': currentAddress_Alias, 'status': status_Alias
}

let orderKeywords = {
    'order_id': orderId_Alias, 'from_date': fromDate_Alias, 'to_date': toDate_Alias,
    'order_comments': orderComment_Alias, 'customer_id': customerId_Alias, 'fulfilled': fulfilled_Alias,
}

let customerKeywords = {

}

class searchService {

    //inclusive search. Will try to match all keywords. keyword && keyword
    bikeSearch(searchTerm, searchPool) {

        let matches = [];
        let term;
        //ignore case
        if (searchTerm)
            term = searchTerm.toString().toLowerCase();
        //first try to find keyvalue pairs
        let keywordValues = parseKeywordValues(term, parseKeywords(term, bikeKeywords), bikeKeywords);

        //if keywords was used in the search
        if (keywordValues.length > 0) {
            searchPool.forEach(bike => {
                let bikeIsMatch = false;

                //get all details of the bike
                let bikeProperties = {
                    id: bike.bike_id, chassieNum: bike.chassienum,
                    name: bike.name, type: bike.type, make: bike.make,
                    model: bike.model, size: bike.size, userLevel: bike.user_level,
                    userGender: bike.user_gender, age: bike.user_age, address: bike.adress,
                    currentAddress: bike.current_adress, status: bike.status, info: bike.bike_info
                }

                //check if any keywords match a property of the bikes
                propLoop:
                for (let prop in bikeProperties) {
                    //if a detail about the bike matches a keyword/keywod alias check if the values match
                    let property = prop.toLowerCase();
                    KeyLoop:
                    for (let i = 0; i < keywordValues.length; i++) {
                        //check only the key that matches the keyword

                        //console.log(`prop: ${property} keyWord: ${keywordValues[i].getKey()}`);
                        if (property === keywordValues[i].getKey())

                            //check if values match the value supplied by keywordvalue
                            if (bikeProperties[prop].toString().toLowerCase() != searchDictionary.getTranslation(keywordValues[i].getKey(), keywordValues[i].getVal())) {
                                bikeIsMatch = false;
                                //if not matching break the parent loop
                                break propLoop;
                            }
                            else
                                bikeIsMatch = true;
                    }
                }
                if (bikeIsMatch)
                    matches.push(bike);
            });
        }
        //if none are found search entire pool for matches to the searchTerm
        else {
            //go through every bike and look for matches to the search input
            searchPool.forEach(bike => {
                let bikeDetails = {
                    id: bike.bike_id, chassieNum: bike.chassienum,
                    name: bike.name, type: bike.type, make: bike.make,
                    model: bike.model, size: bike.size, userLevel: bike.user_level,
                    userGender: bike.user_gender, age: bike.user_age, address: bike.adress,
                    currentAddress: bike.current_adress, status: bike.status, info: bike.bike_info
                }

                for (let key in bikeDetails) {

                    //console.log(bikeDetails.address, bikeDetails[key].toString().toLowerCase(), searchTerm);
                    // if the detail contains the search term add the bike to the matches
                    if (bikeDetails[key].toString().toLowerCase().includes(searchTerm)) {
                        {
                            matches.push(bike);
                            break;
                        }
                    }
                }
            })
        }

        //console.log(matches);
        return matches;
    }

    orderSearch(searchTerm, searchPool) {

        let matches = [];
        //ignore case
        let term = searchTerm.toString().toLowerCase();
        //first try to find keyvalue pairs
        let keywordValues = parseKeywordValues(term, parseKeywords(term, orderKeywords), orderKeywords);

        //console.log("keywords", keywordValues);
        //if keywords was used in the search
        if (keywordValues.length > 0) {
            searchPool.forEach(order => {
                let orderIsMatch = false;

                //get all details of the bike
                let orderPorperties = {
                    order_id: order.order_id, from_date: order.from_date,
                    to_date: order.to_date, to_date: order.to_date, order_comment: order.order_comment,
                    fulfilled: order.fulfilled
                }

                //check if any keywords match a property of the orders
                propLoop:
                for (let prop in orderPorperties) {
                    //if a detail about the order matches a keyword/keywod alias check if the values match
                    let property = prop.toLowerCase();
                    KeyLoop:
                    for (let i = 0; i < keywordValues.length; i++) {
                        //check only the key that matches the keyword

                        //console.log(`prop: ${property} keyWord: ${keywordValues[i].getKey()}`);
                        if (property === keywordValues[i].getKey())

                            //check if values match the value supplied by keywordvalue
                            if (orderPorperties[prop].toString().toLowerCase() != searchDictionary.getTranslation(keywordValues[i].getKey(), keywordValues[i].getVal())) {
                                orderIsMatch = false;
                                //if not matching break the parent loop
                                break propLoop;
                            }
                            else
                                orderIsMatch = true;
                    }
                }
                if (orderIsMatch)
                    matches.push(order);
            });
        }
        //if none are found search entire pool for matches to the searchTerm
        else {
            //go through every bike and look for matches to the search input
            searchPool.forEach(order => {
                let orderPorperties = {
                    order_id: order.order_id, from_date: order.from_date,
                    to_date: order.to_date, to_date: order.to_date, order_comment: order.order_comment,
                    fulfilled: order.fulfilled
                }

                for (let prop in orderPorperties) {

                    //console.log(bikeDetails.address, bikeDetails[key].toString().toLowerCase(), searchTerm);
                    // if the detail contains the search term add the bike to the matches
                    if (orderPorperties[prop].toString().toLowerCase().includes(searchTerm)) {
                        {
                            matches.push(order);
                            break;
                        }
                    }
                }
            });
        }

        //console.log(matches);
        return matches;
    }

    customerSearch(searchTerm, searchPool) {

        let matches = [];
        //ignore case
        let term = searchTerm.toString().toLowerCase();
        //first try to find keyvalue pairs
        let keywordValues = parseKeywordValues(term, parseKeywords(term, customerKeywords), customerKeywords);

        //if keywords was used in the search
        if (keywordValues.length > 0) {
            searchPool.forEach(customer => {
                let customerIsMatch = false;

                //get all details of the bike
                let customerProperties = {
                    order_id: customer.customer_id, name: customer.name, email: customer.email,
                    cell_num: customer.cell_num, date_ob: customer.date_ob, person_id: customer.person_id
                }

                //check if any keywords match a property of the orders
                propLoop:
                for (let prop in customerProperties) {
                    //if a detail about the order matches a keyword/keywod alias check if the values match
                    let property = prop.toLowerCase();
                    KeyLoop:
                    for (let i = 0; i < keywordValues.length; i++) {
                        //check only the key that matches the keyword

                        if (property === keywordValues[i].getKey())

                            //check if values match the value supplied by keywordvalue
                            if (customerProperties[prop].toString().toLowerCase() != searchDictionary.getTranslation(keywordValues[i].getKey(), keywordValues[i].getVal())) {
                                customerIsMatch = false;
                                //if not matching break the parent loop
                                break propLoop;
                            }
                            else
                                customerIsMatch = true;
                    }
                }
                if (customerIsMatch)
                    matches.push(customer);
            });
        }
        //if none are found search entire pool for matches to the searchTerm
        else {
            //go through every bike and look for matches to the search input
            searchPool.forEach(customer => {
                let customerProperties = {
                    order_id: customer.customer_id, name: customer.name, email: customer.email,
                    cell_num: customer.cell_num, date_ob: customer.date_ob, person_id: customer.person_id
                }

                for (let prop in customerProperties) {

                    // console.log(customerProperties[prop], searchTerm);
                    // if the detail contains the search term add the bike to the matches
                    if (customerProperties[prop].toString().toLowerCase().includes(searchTerm)) {
                        {
                            matches.push(customer);
                            break;
                        }
                    }
                }
            });
        }

        //console.log(matches);
        return matches;
    }
}

//Search through searchTerm for keyword or keyword aliases
function parseKeywords(searchTerm, keywords) {
    let matchingKeywords = [];

    //console.log(searchTerm);
    //iterate over every keyword
    for (let keyword in keywords) {
        let aliases = keywords[keyword]
        //iterate over every alias for that keyword.
        //using a regex to prevent false positives such as merida returning match for id
        for (let key in aliases) {

            let alias = aliases[key];
            let regEx = new RegExp(`^\\b${alias}(?:\\s)|(?:\\s)${alias}(?:\\s)|(?:\\s)${alias}^\\D^\\b`, 'g');

            let regResults = regEx.exec(searchTerm);

            if (regResults != null) {
                //we push the main keyword to matches to simplify the process later
                matchingKeywords.push(regResults[0].toLowerCase().trim());
            }
        }
    }

    return matchingKeywords;
}

//get values from the input string and bundle them with their keywords
function parseKeywordValues(searchTerm, keyWords, aliasset) {
    let keyValPairs = []
    for (let index in keyWords) {

        let keyword = keyWords[index];
        let tempString;


        //value string with keyword sliced off.
        tempString = searchTerm.slice(searchTerm.indexOf(keyword) + keyword.length + 1);
        //trim whitespace
        tempString.trim();

        //only slice the string if there is a whitespace
        if (tempString.indexOf(" ") > -1)
            //the value is the first word after the keywords
            tempString = tempString.slice(0, tempString.indexOf(" ") + 1).trim();


        keyValPairs.push(new KeyValuePair(getKeyword(keyword, aliasset), tempString));
    }

    return keyValPairs;
}

//helper to get get original keywords from an alias set
function getKeyword(_alias, aliasset) {

    for (let key in aliasset) {
        for (let alias in aliasset[key]) {
            if (_alias == aliasset[key][alias])
                return aliasset[key][0];    //return the first index since the original keyword is always stored there
        }
    }
}

//keyvalue pair struct
class KeyValuePair {

    constructor(key, val) {
        this.key = key;
        this.val = val;
    }

    getKey() {
        return this.key;
    }

    getVal() {
        return this.val;
    }
}

export let Search = new searchService;
