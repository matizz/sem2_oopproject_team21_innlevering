import { dbSearchHelpers as helper } from "./services_searchDBHelpers";

/**
 * ===TODO===
 * add more aliases
 * add more translations
 * -userLevel
 * -
 */

/**
 * ====ALIASES====
 */

//BikeGender aliases
let maleAliases = ['male', 'mann', 'herre'];
let femaleAliases = ['female', 'dame', 'kvinne'];

//BikeAge aliases
let childAliases = ['child', 'barn', 'kid', 'unge'];
let yuthAliases = ['youth', 'ungdom'];
let adultAliases = [];
let seniorAliases = ['senior', 'voksen', 'gammel', 'adult'];

/**
 * ====DICTIONARIES====
 */

let bikeType = {};
let bikeGender = { 1: maleAliases, 2: femaleAliases };
let bikeAge = { 0: childAliases, 1: yuthAliases, 2: seniorAliases };

class SearchDictionary {

    constructor() {
        this.init();
    }

    //initiate the dictionary
    init() {
        //using helpers to get translation from db when aliasing is not necessar
        //get translations from the db
        helper.GetTypeTranslation((results) => {
            results.forEach((element) => {
                bikeType[element.bike_typeID] = element.bike_type;
            });
        });
    }

    getTranslation(keyword, value) {

        switch (keyword) {
            case ("type"):
                return getTypeTranslation(value);

            case ("usergender"):
                return getGenderTranslation(value);

            case ("age"):
                return getAgeTranslation(value);
            default:
                //if there is no matching keyword translation passthrough the value
                return value;
        }
    }
}

//translates bike_type into bike_typeID
function getTypeTranslation(value) {
    for (let key in bikeType) {

        if (bikeType[key].toLowerCase() == value) {
            return key;
        }
    }
    //if there is not dictonary entry just passthrough the value
    return value;
}

//translate user_gender inot gender
function getGenderTranslation(value) {
    for (let key in bikeGender) {

        //go over all the aliases for genders
        for (let alias in bikeGender[key])

            //if a alias match return the key
            if (bikeGender[key][alias].toLowerCase() == value) {
                return key;
            }
    }
    //if there is not dictonary entry just passthrough the value
    return value;
}

function getAgeTranslation(value) {
    for (let key in bikeAge) {

        //go over all the aliases for genders
        for (let alias in bikeAge[key])

            //if a alias match return the key
            if (bikeAge[key][alias].toLowerCase() == value) {
                console.log("returned:", key);
                return key;
            }
    }
    //if there is not dictonary entry just passthrough the value
    return value;
}

export let searchDictionary = new SearchDictionary();