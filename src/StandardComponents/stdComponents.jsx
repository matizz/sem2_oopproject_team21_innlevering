import * as React from 'react';
import { Component } from 'react-simplified';

export default class PopupWindow extends Component {
  constructor(props) {
    super(props);

    this.state = { wWidth: 0, wHeight: 0 };
  }

  componentDidMount() {
    this.updateWindowDimensions();
    window.addEventListener('resize', this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions);
  }

  //window height and width on a window rezise
  updateWindowDimensions = () => {
    this.setState({ wWidth: window.innerWidth, wHeight: window.innerHeight });
  };

  render() {
    const popupStyle = {
      position: 'absolute',
      overflow: 'auto',
      width: this.props.width,
      height: this.props.height,
      backgroundColor: '#34495e',
      border: '1px solid black',
      top: '0',
      right: '0',
      bottom: '0',
      left: '0',
      opacityBackground: '0.9',
      margin: 'auto',

      boxShadow: '0 0 70px #000000'
    };

    return (
      <div name="PopupWindow" style={popupStyle}>
        {this.props.children}
      </div>
    );
  }
}
