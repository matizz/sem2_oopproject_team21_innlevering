import React from 'react';
import { Component } from 'react-simplified';
import Button from 'react-bootstrap/Button';

class StdButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fontSize: '20px',
      backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : '#34495e',
      border: 'solid 2px',
      borderColor: 'rgba(255,255,255,0.3)',
      color: this.props.color ? this.props.color : 'white',
      margin: this.props.margin,
      marginBottom: '20px',
      marginLeft: '10px',
      //makes it possible to do unique changes on each "thing" using this statement(this.props....).
      marginRight: this.props.marginRight,
      borderRadius: '50px',
      marginTop: this.props.marginTop,
      width: this.props.width,
      height: this.props.height,
      cursor: 'pointer',
      float: this.props.float,
      display: this.props.display,
      position: this.props.position,
      top: this.props.top
    };
  }

  //Changes the colors on the buttons when you leave the button.
  handleMouseLeave = () => {
    this.setState(() => {
      return (
        this.state.backgroundColor === 'rgba(255,255,255,0.3)' && {
          backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : '#34495e',
          color: this.state.color
        }
      );
    });
  };

  //Makes the colors change when you hover over the buttons.
  handleMouseOver = () => {
    this.setState({
      backgroundColor: 'rgba(255,255,255,0.3)',
      color: this.state.color
    });
  };

  render() {
    return (
      <Button
        style={this.state}
        onClick={this.props.handleOnClick}
        onMouseLeave={this.handleMouseLeave}
        onMouseOver={this.handleMouseOver}
      >
        {this.props.children}
      </Button>
    );
  }
}

export default StdButton;
