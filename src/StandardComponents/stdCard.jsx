import React from 'react';
import { Component } from 'react-simplified';

import Card from 'react-bootstrap/Card';

class StdCard extends Component {
  selected = false;
  mouseHover = false;
  mouseDown = false;
  //colors change these to change the color of the card
  backgroundColor = 'white';
  hoverColor = '#A2A1A8';
  mouseDownColor = '#88878f';
  selectedColor = '#A2A1A8';

  //Handle clicks based on what beahviuor is set in props
  handleClick = () => {
    //Selectable behaviour
    if (this.props.selectable) {
      //set all instances of this component to false
      for (let instance of StdCard.instances()) {
        if (instance != this) instance.selected = false;
      }

      //Do onClick
      if (this.props.onClick && !this.selected) this.props.onClick();

      //Handle the selected state of the card
      if (!this.selected) {
        this.selected = true;
        return;
      } else if (this.selected) {
        this.selected = false;
        return;
      }
    }

    //clickable behaviour
    if (this.props.clickable) {
      //Do onClick
      if (this.props.onClick) this.props.onClick();
      return;
    }
  };

  //Graphic functions
  handleMouseOver = () => {
    if (!this.selected && (this.props.selectable || this.props.clickable)) this.mouseHover = true;
  };

  handleMouseLeave = () => {
    this.mouseHover = false;
  };

  handleMouseDown = () => {
    if (this.props.selectable || this.props.clickable) this.mouseDown = true;
  };

  handleMouseUp = () => {
    this.mouseDown = false;
  };

  unSelect = () => {
    console.log('unSelected');
  };

  setBackgroundColor() {
    if (this.selected) return this.selectedColor;
    else if (this.mouseDown) return this.mouseDownColor;
    else if (this.mouseHover) return this.hoverColor;

    return this.backgroundColor;
  }

  render() {
    const style = {
      backgroundColor: this.setBackgroundColor(),
      borderStyle: 'solid',
      borderColor: '#A2A1A8',
      margin: '10px',
      //boxShadow: '6px 6px grey',
      margin: this.props.margin,
      marginTop: this.props.marginTop
    };
    return (
      <Card
        style={style}
        onMouseOver={this.handleMouseOver}
        onMouseLeave={this.handleMouseLeave}
        onMouseDown={this.handleMouseDown}
        onMouseUp={this.handleMouseUp}
        onClick={this.handleClick}
      >
        {this.props.children}
      </Card>
    );
  }
}

export default StdCard;
