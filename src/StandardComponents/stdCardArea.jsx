import React from 'react';
import { Component } from 'react-simplified';

class CardArea extends Component {
  constructor(props) {
    super(props)
    this.state = {
      overflowY: this.props.overflowY ? this.props.overflowY : 'auto',
      backgroundColor: '#34495e',
      opacityBackground: '0.9',
      margin: '10px',
      width: this.props.width,
      height: this.props.height,
      boxShadow: 'inset 0 0 70px #000000'
    }
  }

  render() {
    return (
      <div style={this.state}>
        {this.props.children}
      </div >
    )
  }
}

export default CardArea;
