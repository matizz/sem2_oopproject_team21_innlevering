import React from 'react';
import { Component } from 'react-simplified';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col'
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button'

class Search extends Component {

  input = '';

  handleChange = event => {
    this.input = event.target.value;

    if (this.props.onSubmit)
      this.props.onSubmit(this.input.toLowerCase());
  };

  render() {
    const searchBarStyle = {
      marginLeft: '30px'
    };

    return (
      <Form inline style={searchBarStyle} onSubmit={this.handleSubmit}>
        <Row>
          <Col >
            <FormControl type="text" placeholder="Search" className="mr-sm-2" onChange={this.handleChange} />
          </Col>
        </Row>
      </Form>
    );
  }
}

export default Search;
