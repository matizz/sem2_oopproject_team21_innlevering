import {
    connection
} from '../Services_MYSQL/service_mysqlConnection';


class DatabaseService {

    getCustomers(success) {
        connection.query('select * from customers', (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    addNewCustomer(fName, lName, email, cell_num, date_ob, person_id, success) {
        connection.query('insert into customers (name, email, cell_num, date_ob, person_id) values (?,?,?,?,?)', [fName + " " + lName, email, cell_num, date_ob, person_id], (error, results) => {
            if (error) return console.log(error);

            success(results);
        })
    }

    getCustomer(id, success) {
        connection.query('select * from customers where customer_id=?', [id], (error, results) => {
            if (error) return console.error(error);

            success(results[0]);
        })
    }

    updateCustomer(id, name, email, cell_num, date_ob, person_id, success) {
        connection.query('update customers set name=?, email=?, cell_num=?, date_ob=?, person_id=? where customer_id=?', [name, email, cell_num, date_ob, person_id, id], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }




}
export let dbServiceCustomers = new DatabaseService();