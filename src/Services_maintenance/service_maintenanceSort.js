import { dateService } from './../Services_Status/services_date';
var arraySort = require('array-sort');

class MaintenanceSort {

    sortByOldest(array) {
        let tempArray = tempArray = IndexArrayWithDate(array);
        let sortedArray = [];

        tempArray = arraySort(tempArray, 'sortIndex');

        tempArray.forEach(element => {
            let rep = element.element;

            sortedArray.push(rep);
        });

        return sortedArray
    }

    sortByNewest(array) {
        let tempArray = tempArray = IndexArrayWithDate(array);
        let sortedArray = [];

        tempArray = arraySort(tempArray, 'sortIndex', { reverse: true });

        tempArray.forEach(element => {
            let rep = element.element;

            sortedArray.push(rep);
        });

        return sortedArray
    }
}

function IndexArrayWithDate(array) {
    let tempArray = [];
    array.forEach(element => {
        let sortIndex = dateService.sqlDateToYear(element.from_date);
        let elementWithIndex = {}
        elementWithIndex.sortIndex = sortIndex;
        elementWithIndex.element = element;
        tempArray.push(elementWithIndex);
    });

    return tempArray;
}

export let maintenanceSort = new MaintenanceSort();