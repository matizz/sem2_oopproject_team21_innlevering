import {
  connection
} from '../Services_MYSQL/service_mysqlConnection';
import date from "date-and-time";

class DatabaseService {
  now = new Date();
  currentDate = date.format(this.now, "YYYY-MM-DD-");

  maintenanceDone(id, success) {
    connection.query('update repairs set to_date=? where bike_id=?', [this.currentDate, id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  updatebikeFixed(id, success) {
    connection.query('update bikes set status=1 where bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  updateBikeScrapped(id, success) {
    connection.query("update bikes set status=4 where bike_id=?", [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    })
  }


  getMaintenanceBikes(success) {
    connection.query('SELECT * from bikes, repairs, status where bikes.bike_id = repairs.bike_id and bikes.status = status.status_id and repairs.to_date is null', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getOldRepairs(success) {
    connection.query('select * from bikes, repairs, status where bikes.bike_id = repairs.bike_id and bikes.status = status.status_id and repairs.to_date is not null', (error, results) => {
      if (error) return console.error(error);

      success(results);
    })
  }


}
export let dbServiceMaintenance = new DatabaseService();