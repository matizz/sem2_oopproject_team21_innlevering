import {
  connection
} from '../Services_MYSQL/service_mysqlConnection';
import date from "date-and-time";




class DatabaseService {
  now = new Date();
  currentDate = date.format(this.now, "YYYY-MM-DD");

  updateBikeStatusMaintenance(id, success) {
    connection.query('update bikes set status=3 where bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getBikeType(success) {
    connection.query('select bike_typeID, bike_type from bike_type', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getGender(success) {
    connection.query('select gender_id, gender_name from user_gender ', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }
  getUserAge(success) {
    connection.query('select user_age, age from user_age ', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }


  getUserLevel(success) {
    connection.query('select distinct user_level from bikes order by user_level', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  //queries for edit bike
  getCurrentBikeType(id, success) {
    connection.query('select bike_typeID, bike_type from bike_type, bikes where bikes.type= bike_type.bike_typeID and bikes.bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getCurrentGender(id, success) {
    connection.query('select user_gender from bikes where bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }
  getCurrentUserAge(id, success) {
    connection.query('select user_age.user_age, user_age.age from user_age, bikes where user_age.user_age = bikes.user_age and bikes.bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }


  getCurrentUserLevel(id, success) {
    connection.query('select user_level from bikes where bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  bikeNeedsMaintenance(rep, id, success) {
    connection.query(
      "insert into repairs (from_date, rep_description, bike_id) values (?, ?, ?)",
      [this.currentDate, rep, id],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );
  }

  addNewBike(chassienum, name, type, make, model, size, user_level, user_gender, user_age, bike_info, success) {
    connection.query(
      'insert into bikes (chassienum, name, type, make, model, size, user_level, user_gender, user_age, adress, current_adress, status, bike_info) values(?,?,?,?,?,?,?,?,?,"trondheim","trondheim","1",?)',
      [chassienum, name, type, make, model, size, user_level, user_gender, user_age, bike_info],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );
  }

  removeBike(id, success) {
    connection.query('update bikes set status=4 where bike_id=?', [id], (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  getForeignBike(id, success) {
    connection.query(
      'select * from bikes, locations where locations.adress = bikes.adress and status not like 4 and chassienum=?',
      [id],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );

  }

  addForeignBike(id, success) {
    connection.query(
      'select adress from users where username = ?',
      [localStorage.getItem('loginUser')],
      (error, results) => {
        if (error) return console.error(error);
        let address = results[0].adress;
        connection.query(
          'update bikes set current_adress= ? where chassienum=?',
          [address, id],

          error => {
            if (error) {
              success(false);
              return console.error(error);
            }

            success(true);
          }
        );
      }
    );
  }

  getBike(id, success) {
    connection.query('select * from bikes where bike_id=?', [id], (error, results) => {
      if (error) console.error(error);

      success(results[0]);
    })
  }
}
export let dbServiceBikes = new DatabaseService();