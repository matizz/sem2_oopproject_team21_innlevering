import { dbService } from './../Services_MYSQL/service_mysqlQueries';

const bcrypt = require('bcryptjs');

class LoginService {

    HandleLogin(username, password, callBack = () => { }) {
        dbService.GetPassword(username, (passwordHash = " ") => {
            let status = "";
            // bcrypt.hash("pwdhaugastøl", 10, function (err, hash) {
            //     console.log(hash);
            // });
            if (bcrypt.compareSync(password, passwordHash)) {
                status = `Welcome ${username}`;


                localStorage.setItem('loginUser', username);
                localStorage.setItem('loginStatus', true);

                callBack(status, 1);
                return;
            }
            else {
                status = "wrong username or password";
                callBack(status, 0);
            }

        });
    }

    HandleLogout(callBack = () => { }) {
        localStorage.setItem('loginUser', " ");
        localStorage.setItem('loginStatus', false);
        callBack();
    }

    VerifySession(timeSinceLastLogin) {
        console.log(Date.now() - timeSinceLastLogin);

        if ((Date.now() - timeSinceLastLogin) > 5000000) {
            console.log("session to old");

            localStorage.setItem('loginUser', " ");
            localStorage.setItem('loginStatus', false);
        }
    }
}

export let loginService = new LoginService();