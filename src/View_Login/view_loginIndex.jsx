import React from 'react';
import { Component } from 'react-simplified';
import createHashHistory from 'history/createHashHistory';
import { loginService } from './../Services_Login/Service_login';
import PopupWindow from '../StandardComponents/stdComponents';
//bootstrap import
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import StdButton from './../StandardComponents/stdButton';
const history = createHashHistory();
const remote = require('electron').remote;

//main component of the login page
export default class Login extends Component {

    onClickExit = () => {
        let window = remote.getCurrentWindow();
        window.close();
    }

    render() {
        //style of the general login page
        const LoginPageStyle = {
            height: "100vh",    //vh is relative to 1% of the height of the viewport 100vh is the total height of the viewport
            backgroundImage: "url('../public/img/LoginPage_BackgroundImg.jpg')",
            backgroundSize: "cover",
        }

        //The history object is passed through all the components down to the login form
        return (
            <div style={LoginPageStyle} >
                <StdButton handleOnClick={this.onClickExit}>Exit</StdButton>
                <LoginHeader />
                <Container >
                    <Row>
                        <Col md={4}></Col>
                        <Col md={4}>
                            <Card className="text-center" style={{ width: '18rem' }}>
                                <Card.Body>
                                    <LoginForm />
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md={4}></Col>
                    </Row>
                </Container>
            </div >
        )
    }
}

//Header component for the login page.
class LoginHeader extends Component {
    render() {
        //the overall header style
        const LoginHeaderStyle = {
            height: "20%",
            paddingTop: "50px",
            textAlign: "center",
            fontSize: "40px",
            fontFamily: 'Roboto, sans-serif',
            fontStyle: " italic",
        };

        return (
            <div name="LoginHeader" style={LoginHeaderStyle}>
                Welcome to Cyclease Manager 2019™
            </div >
        )
    }
}

class LoginForm extends Component {
    userName = '';
    password = '';
    loginStatusMessage = '';
    loginStatusMessageColor = '';

    //handles changes to the form, updating the username and password state
    handleChange = (event) => {
        if (event.target.type == "text")
            // this.setState({ userName: event.target.value });
            this.userName = event.target.value;
        else
            //this.setState({ password: event.target.value });
            this.password = event.target.value;

        this.loginStatusMessage = '';

    }

    //handles the submit event, passing the request on to the login service
    handleSubmit = (event) => {

        event.preventDefault();
        loginService.HandleLogin(this.userName, this.password, (statusMsg, status) => {

            this.loginStatusMessage = statusMsg;
            if (status == 1) {
                //this.setState({ loginStatusMessageColor: '#466b02' });
                this.loginStatusMessageColor = '#466b02';
                setTimeout(() => history.push('/App'), 1000);
            }
            else
                //this.setState({ loginStatusMessageColor: '#f71d21' });
                this.loginStatusMessageColor = '#f71d21'
        });
    }

    render() {
        const formStyle = {
            textAlign: "left",
        }
        const loginStatusStyle = {
            margin: "0.5em",

            textAlign: "center",
            fontFamily: 'Roboto, sans-serif',
            fontSize: "20px",
            color: this.loginStatusMessageColor,
        };
        return (
            <div style={formStyle}>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group controlId="LoginUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="Username" placeholder="Username" onChange={this.handleChange} />
                    </Form.Group>

                    <Form.Group controlId="LoginPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={this.handleChange} />
                    </Form.Group>
                    <div style={{ textAlign: "center" }}>
                        <Button variant="primary" type="submit" >
                            Submit
                        </Button>
                    </div>

                    <div style={loginStatusStyle}>
                        {this.loginStatusMessage}
                    </div>
                </Form>
            </div>
        );
    }
}