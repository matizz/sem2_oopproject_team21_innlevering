import React from 'react';
import { Component } from 'react-simplified';
//imports db-queries
import { dbServiceTransport } from '../../Services_transport/service_transportQueries';
//imports standardcomponents
import StdCards from '../../StandardComponents/stdCard';
import CardArea from '../../StandardComponents/stdCardArea';
import StdButton from '../../StandardComponents/stdButton';
//imports bootstrap
import Form from 'react-bootstrap/form';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
//imports package that shows loading-icon while waiting for db
import ReactLoading from 'react-loading';

const bikeInfoAreaStyle = {
  marginTop: '10px',
  textAlign: 'center',
  height: '50px',
  width: '100%',
  maxHeight: '50vh',
  padding: '0'
};

const infoDiv = {
  borderStyle: 'solid',
  border: 'solid rgba(0,0,0,0.3) 2px',
  margin: 'auto',
  textAlign: 'center',
  padding: '0',
  marginTop: '15px',
  width: '100%',
  color: 'white',
  marginLeft: '15px',
  marginRight: '15px'
};

class Transports extends Component {
  //arrays containing data from db
  allBikes = [];
  filteredBikes = [];
  cities = [];
  transportInfo = [];
  //selected city from drop-down
  selectedCity = '';
  //Flags
  gettingBikes = false;
  simulateTransportCurrentAdress = '';
  simulateTransportAdress = '';
  resultSimulateTransport = [];

  //handler when changing value in dropdown
  cityChanged() {
    //transport-info for selected city
    dbServiceTransport.getCityInfo(this.selectedCity, transportInfo => {
      this.transportInfo = transportInfo;
      this.filteredBikes = this.allBikes.filter(bike => {
        return bike.zip_location === this.selectedCity;
      });
      //only change variable if there are bikes in array
      if (this.filteredBikes.length > 0) {
        this.simulateTransportCurrentAdress = this.filteredBikes[0].current_adress;
        this.simulateTransportAdress = this.filteredBikes[0].adress;
      }
    });
  }

  //filter bikes based on the new selected city
  simulateTransport() {
    //query when "sending" bikes back where they belong
    dbServiceTransport.simulateTransport(
      this.simulateTransportAdress,
      this.simulateTransportAdress,
      resultSimulateTransport => {
        this.resultSimulateTransport = resultSimulateTransport;
      }
    );
    this.mounted();
  }

  render() {
    let bikeCards;

    const selector = {
      marginLeft: '30px',
      width: '25%'
    };

    //if there are bikes that can be shown
    if (!this.gettingBikes && this.filteredBikes.length > 0)
      bikeCards = this.filteredBikes.map(item => (
        <StdCards key={item.bike_id} transport={item} margin="15px">
          <Card.Header>
            <h1>{item.make}</h1>
          </Card.Header>
          <Card.Body>
            <Container>
              <Row>
                <Col sm={4}>
                  <a>Bike id:</a>
                  <br />
                  <a>Origin:</a>
                </Col>
                <Col sm={8}>
                  {item.bike_id}
                  <br />
                  {item.zip_location}
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </StdCards>
      ));
    else if (this.gettingBikes)
      //show a spinner while the query runs
      bikeCards = (
        <div width="100%" style={{ width: '13.5%', margin: '20vh auto' }}>
          <ReactLoading type="spin" color="black" delay={250} />
        </div>
      );
    //shows that no bikes needs to be moved to the selected city, if you dont have any of that locations bikes.
    else
      bikeCards = (
        <div style={{ textAlign: 'center', color: 'white', margin: '20vh auto' }}>No bikes need transport</div>
      );

    return (
      <Container fluid={true}>
        <Row>
          <Col>
            <Form>
              <Form.Group style={{ margin: '0' }} controlId="exampleForm.ControlSelect1">
                <Form.Control
                  as="select"
                  onChange={event => {
                    this.selectedCity = event.target.value;
                    this.cityChanged();
                  }}
                  style={selector}
                >
                  {this.cities.map(city => (
                    <option key={city.zip_location} value={city.zip_location}>
                      {city.zip_location}
                    </option>
                  ))}
                </Form.Control>
              </Form.Group>
            </Form>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <CardArea height="80vh" width="100%">
              {bikeCards}
            </CardArea>
          </Col>
          <Col md={6}>
            <Row>
              <div style={infoDiv}>
                <div>
                  <h3 style={bikeInfoAreaStyle}>Transport info: {this.selectedCity}</h3>
                </div>
                <blockquote className="blockquote mb-0" style={{ textAlign: 'center' }}>
                  {this.transportInfo.map(info => (
                    <p key={info.info}>{info.info}</p>
                  ))}
                </blockquote>
              </div>
            </Row>
            <Row>
              <Col style={{ textAlign: 'center', top: '120px' }}>
                <StdButton width="300px" height="40px" handleOnClick={this.simulateTransport}>
                  Mark all bikes as picked up
                </StdButton>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }

  mounted() {
    // Get all bikes that does not belong at this users adress:
    this.gettingBikes = true;
    dbServiceTransport.getBikesCity(bikes => {
      this.allBikes = bikes;
      this.gettingBikes = false;
    });

    // Get all cities in the system:
    dbServiceTransport.getCities(cities => {
      this.cities = cities;
      this.selectedCity = this.cities[0].zip_location;

      // Get transport info from the initial selected city:
      dbServiceTransport.getCityInfo(this.selectedCity, transportInfo => {
        this.transportInfo = transportInfo;
        this.cityChanged();
      });
    });
  }
}

export default Transports;
