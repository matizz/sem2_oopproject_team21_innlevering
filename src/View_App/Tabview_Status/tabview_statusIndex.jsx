import React from 'react';
import { Component } from 'react-simplified';
import createHashHistory from 'history/createHashHistory';
import { statusService } from '../../Services_Status/services_status';
import StdCard from './../../StandardComponents/stdCard';
import Card from 'react-bootstrap/Card';
import CardColumns from 'react-bootstrap/CardColumns';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from "react-bootstrap/Col";
const history = createHashHistory();

export default class StatusIndex extends Component {
    allNotifications = [];

    render() {
        return (
            <Container fluid>
                <Row>
                    <Col>
                        <CardColumns>
                            <AlertCards notifications={this.allNotifications} />
                        </CardColumns>
                    </Col>
                </Row>
            </Container>
        );
    };

    mounted() {
        statusService.queryNotifications(result => {
            this.allNotifications = result;
        });
    };
}

class AlertCards extends Component {

    handleOnclick = () => {
        //send user to apropriate page
        if (this.props.alert.type === "orderConflict")
            history.push("/App/Orders");
    }

    render() {
        let card;

        card = this.props.notifications.map(notifications => {
            return notifications.length > 0 && (
                <StdCard key={this.props.notifications.indexOf(notifications)} margin="5px" onClick={this.handleOnclick} >
                    <Card.Header> {notifications[0].title}</Card.Header>
                    <Card.Body>
                        <ul>
                            {notifications.map((note, i) => (
                                <li key={i}>
                                    {note.description};
                            </li>
                            ))}
                        </ul>
                    </Card.Body>
                </StdCard >
            )
        });

        return (
            <React.Fragment>
                {card}
            </React.Fragment>
        )
    }
}
