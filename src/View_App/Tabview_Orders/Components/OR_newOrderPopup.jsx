import React from 'react';
import { Component } from 'react-simplified';
import date from "date-and-time";
// Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
// Componenter
import CardArea from '../../../StandardComponents/stdCardArea';
import StdButton from '../../../StandardComponents/stdButton';
import StdCard from '../../../StandardComponents/stdCard';
import PopupWindow from '../../../StandardComponents/stdComponents';
// Queries
import { orderService } from '../../../Services_orders/service_orderQueries';

const infoStyle = {
  marginTop: '10px',
  textAlign: 'center',
  height: '50px',
  width: "100%",
  maxHeight: '50vh',
  color: 'white',
  padding:"0"
};

class NewOrder extends Component {
  constructor(props) {
    super(props);

    this.state = {
      // Date:
      from: '',
      to: '',

      // Bikes:
      removeBike: [],
      bikes: [],
      list: [],
      showBikeInfo: false,
      showBikeInfoID: [],

      // Equipment:
      equipment: [],
      addEq: [],
      addedEquip: [],

      orderID: [],
      page: 1,
      location: '',

      // Customer:
      firstName: '',
      lastName: '',
      dob: '',
      phone: '',
      email: '',
      personId: '',
      customers: [],
      customerID: [],
      comment: '',
      selectedCustomer: [],

      // Query checks:
      newCustomer: false,
      newOrder: false
    };
  }

  // Adds equipment to order:
  addEquip = id => {
    orderService.selectEquipment(id, item => {
      this.setState({
        addEq: item
      });

      this.setState(state => {
        const addedEquip = state.addedEquip.concat(state.addEq);

        return {
          addedEquip,
          addEq: []
        };
      });
    });
  };

  // Removes equipment from order:
  removeEquip = index => {
    this.setState(state => {
      const addedEquip = state.addedEquip.filter(item => this.state.addedEquip.indexOf(item) !== index);

      return {
        addedEquip
      };
    });
  };

  // Adds bike to order:
  addBike = id => {
    this.setState(state => {
        const list = state.list.concat(this.state.bikes.filter(item => item.bike_id === id));
        const bikes = state.bikes.filter(bike => bike.bike_id !== id);

        return {
            list,
            bikes,
        };
    });
  };

  // Removes bike from order:
  removeBike = id => {
    this.setState(state => {
      const list = state.list.filter(bike => bike.bike_id !== id);
      const removeBike = state.list.filter(bike => bike.bike_id === id);

      return {
        list,
        removeBike
      };
    });
    this.updateBike();
  };

  // Updates the bike-component when a bike is removed from the order:
  updateBike = () => {
    this.setState(state => {
      const newBikes = state.bikes.concat(state.removeBike);
      const bikes = newBikes.sort((a, b) => {
        return a.bike_id - b.bike_id;
      });

      return {
        bikes,
        removeBike: []
      };
    });
  };

  // Next page:
  nextPage = () => {
    let prevState = this.state.page;
    if (this.state.page === 1 && (this.state.from === '' || this.state.to === '')) {
      alert('Du må fylle inne fra- og til-dato')
    } else {
      if (this.state.page === 1) {
        this.getAvailableBikes()
      }
      this.setState({
        page: prevState + 1
      });
    }
    
  };

  // Previous page:
  prevPage = () => {
    let prevState = this.state.page;
    if (this.state.page === 5) {
      this.setState({
        page: 3
      });
    } else {
      this.setState({
        page: prevState - 1
      });
    }
  };

  // Taker the user to the "Existing Customer"-page:
  existingCustomer = () => {
    this.setState({
      page: 4
    });
  };

  // Takes the user to the "New Customer"-page:
  newCustomer = () => {
    this.setState({
      page: 5
    });
  };

  // Adds the correct values to state when the user inserts something into the inputs:
  setValue = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  // Creates a new customer or makes a call for createOrder() depending on what page you're on:
  saveOrder = () => {
    // Checks page
    if (this.state.page === 5) {
      // Creates new customer
      orderService.orderNewC(
        this.state.firstName,
        this.state.lastName,
        this.state.dob,
        this.state.phone,
        this.state.email,
        this.state.personId,
        this.getCustomerID
      );
    } else if (this.state.page === 4) {
      // Continues to the createOrder()-method:
      this.createOrder()
    }
  };

  // If a new customer is created, we make a call for the customer ID:
  getCustomerID = () => {
    // Gets the customer ID and adds it to state
    orderService.getCustomerID(id => {
      this.setState(
        {
          customerID: id
        },
        this.createOrder
      );
    });
  };

  // Creates a new order with the customer ID:
  createOrder = () => {
    orderService.newOrder(
      this.state.from,
      this.state.to,
      this.state.comment,
      this.state.customerID[0].customer_id,
      0,
      this.getOrderId
    );
  };

  // Gets the order ID and adds it to state:
  getOrderId = () => {
    orderService.getOrderID(item => {
      this.setState(
        {
          orderID: item
        },
        this.finishOrder
      );
    });
  };

  // Counts how many of each type of equipment that is selected:
  countEq = () => {
    // Counted equipment:
    let eqArray = [];
    // Unique IDs:
    let ids = [];
    // Count of each unique equipment:
    let counts = [];
    let count = 0;

    // Sorts addedEquip by id:
    this.setState(() => {
      const addedEquip = this.state.addedEquip.sort((a, b) => {
        if (a.eq_id !== b.eq_id) {
          return a.eq_id - b.eq_id;
        }
        if (a.name === b.name) {
          return 0;
        }
      });
      return addedEquip;
    });

    for (let i = 0; i < this.state.addedEquip.length; i++) {
      // The id exists inn eqArray:
      if (eqArray.some(x => x.eq_id === this.state.addedEquip[i].eq_id)) {
        count = count + 1;
        eqArray.push(this.state.addedEquip[i]);
        // Push the count if its the last item:
        if (i === this.state.addedEquip.length - 1) {
          counts.push(count);
        }
        // The id does not exist in eqArray:
      } else {
        ids.push(this.state.addedEquip[i].eq_id);
        eqArray.push(this.state.addedEquip[i]);
        if (count === 0) {
          count = 1;
        } else {
          counts.push(count);
          count = 1;
          // Push the count if its the last item:
          if (i === this.state.addedEquip.length - 1) {
            counts.push(count);
          }
        }
      }
    }

    // Does a query for each equipment with its unique amount:
    for (let i = 0; i < ids.length; i++) {
      orderService.orderEquipment(ids[i], this.state.orderID[0].order_id, counts[i]);
    }
  };

  // Maked sure the order is connected to the correct bikes and makes a call for countEq():
  finishOrder = () => {
    this.countEq();
    // Creates a bike_order item for each bike in the order
    for (let i = 0; i < this.state.list.length; i++) {
      orderService.orderBike(this.state.list[i].bike_id, this.state.orderID[0].order_id);
    }
    this.props.toggleNewOrderPopup()
  };

  // Retrieves the selected customer:
  selectCustomer = id => {
    orderService.selectCustomer(id, results => {
      this.setState({
        customerID: results
      })
    })
  };

  // Toggles "Bike info"-popup:
  toggleBikeInfo = id => {
    this.setState({
      showBikeInfo: !this.state.showBikeInfo,
      showBikeInfoID: id
    })
  }

  // Retrieves available bikes for chosen timeperiod:
  getAvailableBikes = () => {
    orderService.getAvailableBikes(this.state.location[0].adress, this.state.from, this.state.to, item => {
      this.setState({
        bikes: item
      })
    })
  }

  render() {
    // Sorts all available bikes alphabeticaly:
    const sortedBikes = this.state.bikes.sort((a, b) => {
      if (a.make !== b.make) {
          if(a.make < b.make) { return -1; }
          if(a.make > b.make) { return 1; }
          return 0;
      } else {
          if(a.name < b.name) { return -1; }
          if(a.name > b.name) { return 1; }
          return 0;
      }   
    })

    // Component of available bikes:
    const bikesComponent = sortedBikes.map(item => (
      <StdCard margin='10px' key={item.bike_id}>
        <Card.Header>
          <h3>{item.make} {item.name}</h3>
        </Card.Header>
        <Card.Body>
          <Container>
            <Row>
              <Col>
                <StdButton width="100px" height="40px" handleOnClick={() => {this.toggleBikeInfo(item.bike_id)}}>
                  Info
                </StdButton>
              </Col>
              <Col>
                <StdButton   width="100px" height="40px" handleOnClick={() => {this.addBike(item.bike_id)}}>
                  Add
                </StdButton>
              </Col>
            </Row>
          </Container>
        </Card.Body>
        
      </StdCard>
    ));
    
    // Component of bikes that are added to the order:
    const addedBikesComponent = this.state.list.map(item => (      
      <StdCard key={item.bike_id} margin="10px">
        <Card.Header>
          <Row>
            <Col sm="8">
              <h3>{item.make} {item.name}</h3>
              <a>Size: {item.size}</a> <br/>
              <a>Bike ID: {item.bike_id}</a> <br/>
            </Col>
            <Col sm="4">
              <StdButton width="40px" height="40px" handleOnClick={() => {this.removeBike(item.bike_id)}} id={item.bike_id}>
                X
              </StdButton>
            </Col> 
          </Row>    
        </Card.Header>
      </StdCard>
    ));
    
    // Component of equipment:
    const equipComponent = this.state.equipment.map(item => (
      <StdCard margin='10px' key={item.eq_id}>
        <Card.Header>
          <Row>
            <Col sm="8">
              <h3>{item.name}</h3>
            </Col>
            <Col sm="4">
              <StdButton height="50px" width="80px" marginTop="10px"
                handleOnClick={() => {
                  this.addEquip(item.eq_id);
                }}
              >
                Add
              </StdButton>
            </Col>
          </Row>    
        </Card.Header>
      </StdCard>
    ));

    // Component of equipment that is added to the order:
    const addedEquipComponent = this.state.addedEquip.map(item => (
      <StdCard margin="10px" key={this.state.addedEquip.indexOf(item)}>
        <Card.Header>
          <Row>
            <Col sm="8">
              <h3>{item.name}</h3> 
            </Col>
            <Col sm="4">
              <StdButton height="40px" width="40px" marginTop="13px"
                handleOnClick={() => {
                  this.removeEquip(this.state.addedEquip.indexOf(item));
                }}
              >
                X
              </StdButton>
            </Col>
          </Row>
        </Card.Header>
      </StdCard>
    ));
    
    // Component of all costumers:
    const costumerComponent = this.state.customers.map(item => (
      <StdCard selectable='true' onClick={() => this.selectCustomer(item.customer_id)} margin='10px' key={item.person_id}>
        <Card.Header>
          <h3>{item.name}</h3>
        </Card.Header>
        <Card.Body>
          <a>
            Email: {item.email} <br />
            Phone: {item.cell_num} <br/>
            Customer ID: {item.customer_id} <br/>
            Date of Birth: {date.format(new Date(item.date_ob), "DD/MM-YYYY")} <br/>
            Person ID: {item.person_id}
          </a>
        </Card.Body>
      </StdCard>
    ));
    
    // A component of info about the chosen bike:
    const bikeInfoComponent = this.state.bikes.filter(x => x.bike_id === this.state.showBikeInfoID).map(item => (
      <StdCard margin="10px" key={item.bike_id}>
        <Card.Header>
          <h3>{item.make} {item.name}</h3>
        </Card.Header>
        <Card.Body>
          {item.bike_info}
        </Card.Body>
        <Card.Footer>
          <a>
            User level: {item.user_level}<br/>
            Chassie number: {item.chassienum} <br/>
            Model: {item.model}
          </a> 
        </Card.Footer>
      </StdCard>
    ))

    return (
      <div>
        {/* Select Date */}
        {this.state.page === 1 && (
          <Container>
            <Row>
              <Col sm="3"></Col>
              <Col sm="6">
                <StdCard marginTop="155px">
                  <Card.Header>
                    <h3>Date</h3>
                  </Card.Header>
                    <Card.Body>
                      <Row>
                        <Col sm="4">
                          <a>From:</a> <br/> <br/>
                          <a>To:</a>
                        </Col>
                        <Col sm="8">
                            <Form.Group  >  
                              <Form.Control
                                type="date"
                                name="from"
                                onChange={this.setValue} 
                                value={this.state.from}
                              />
                            </Form.Group>
                            <Form.Group>                            
                                <Form.Control
                                  type="date"
                                  name="to"
                                  onChange={this.setValue} 
                                  value={this.state.to}
                                />
                            </Form.Group>                                         
                        </Col>
                      </Row>                     
                  </Card.Body>
                </StdCard>
              </Col>
              <Col sm="3"></Col>
            </Row>
            <Row>
              <Col sm="4"></Col>
              <Col sm="4"></Col>
              <Col sm="4">
                <StdButton marginTop="158px" handleOnClick={this.props.toggleNewOrderPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton marginTop="158px" handleOnClick={this.nextPage} width="100px" height="50px">
                  Next
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* Add Bikes */}
        {this.state.page === 2 && (
          <Container>
            <Row>
              <Col sm="6">
                <CardArea height="500px" width="100%">
                  {bikesComponent}
                </CardArea>
              </Col>
              <Col sm="6">
                <Row>
                  <CardArea height="300px" width="100%">
                    {addedBikesComponent}
                  </CardArea>
                </Row>
                <Row>
                  <Col sm="7"></Col>
                  <Col sm="5">
                    <StdButton marginTop="140px" handleOnClick={this.prevPage} width="100px" height="50px">
                      Previous
                    </StdButton>
                  </Col>
                </Row>            
              </Col>
            </Row>
            <Row>
              <Col sm="4"></Col>
              <Col sm="4"></Col>
              <Col sm="4">
                <StdButton handleOnClick={this.props.toggleNewOrderPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton handleOnClick={this.nextPage} width="100px" height="50px">
                  Next
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* Add Equipment */}
        {this.state.page === 3 && (
          <Container>
            <Row>
              <Col sm="6">
                <CardArea height="500px" width="100%">
                  {equipComponent}
                </CardArea>
              </Col>
              <Col sm="6">
                <Row>
                  <CardArea height="300px" width="100%">
                    {addedEquipComponent}
                  </CardArea>
                </Row>
                <Row>
                  <Col sm="4"></Col>
                  <Col sm="8">
                    <StdButton marginTop="65px" handleOnClick={this.existingCustomer} width="200px" height="50px">
                      Existing Customer
                    </StdButton>
                    <StdButton handleOnClick={this.newCustomer} width="200px" height="50px">
                      New Customer
                    </StdButton>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col xs="6" sm="4"></Col>
              <Col xs="6" sm="4"></Col>
              <Col sm="4">
                <StdButton marginTop="5px" handleOnClick={this.props.toggleNewOrderPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton marginTop="5px" handleOnClick={this.prevPage} width="100px" height="50px">
                  Previous
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* Existing Customer */}
        {this.state.page === 4 && (
          <Container>
            <Row>
              <Col xs="6">
                <CardArea height="500px" width="100%">
                  {costumerComponent}
                </CardArea>
              </Col>
              <Col xs="6">
                <Row>
                  <CardArea height="300px" width="100%">
                    <div style={infoStyle}>
                      <h4>Bikes:</h4>
                    </div>
                    {addedBikesComponent}
                    <div style={infoStyle}>
                      <h4>Equipment: </h4>
                    </div>
                    {addedEquipComponent}
                  </CardArea>
                </Row>
                <Row>
                  <Col>
                    <Form.Group>
                      <Form.Label>
                        <a style={{color: 'white'}}>Order Comment:</a>
                      </Form.Label>
                      <Form.Control name="comment" as="textarea" rows="2" placeholder="Insert text..." value={this.state.comment} onChange={this.setValue} />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Col sm="4">           
                  </Col>
                  <Col sm="8">
                    <br/>
                    <StdButton handleOnClick={this.saveOrder} width="200px" height="50px">
                      Save Order
                    </StdButton>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col xs="6" sm="4"></Col>
              <Col xs="6" sm="4"></Col>
              <Col sm="4">
                <StdButton handleOnClick={this.props.toggleNewOrderPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton handleOnClick={this.prevPage} width="100px" height="50px">
                  Previous
                </StdButton>
              </Col>
              
            </Row>
          </Container>
        )}
        {/* New Customer */}
        {this.state.page === 5 && (
          <Container>
            <Row>
              <Col>
                <Form>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>First Name: </a></Form.Label>
                    <Col sm="8">
                      <Form.Control type="text" name="firstName" value={this.state.firstName} onChange={this.setValue} placeholder="Ola" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Last Name:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control type="text" name="lastName" value={this.state.lastName} onChange={this.setValue} placeholder="Nordmann" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Date of Birth:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control type="date" name="dob" value={this.state.dob} onChange={this.setValue} />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Phone:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control type="text" name="phone" value={this.state.phone} onChange={this.setValue} placeholder="999 99 999"/>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Email:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control type="text" name="email" value={this.state.email} onChange={this.setValue} placeholder="ola@nordmann.no"/>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Person ID:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control type="text" name="personId" value={this.state.personId} onChange={this.setValue} placeholder="12345678"/>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row}>
                    <Form.Label column sm="4"><a style={{color: 'white'}}>Order Comment:</a> </Form.Label>
                    <Col sm="8">
                      <Form.Control as="textarea" rows="2" name="comment" value={this.state.comment} onChange={this.setValue} placeholder="Insert text..."/>
                    </Col>
                  </Form.Group>
                </Form>
              </Col>
              <Col>
                <Row>
                  <CardArea height="300px" width="100%">
                    <div style={infoStyle}>
                      <h4>Bikes:</h4>
                    </div>
                    {addedBikesComponent}
                    <div style={infoStyle}>
                      <h4>Equipment: </h4>
                    </div>
                    {addedEquipComponent}
                  </CardArea>
                </Row>
              </Col>
            </Row>
            <Row>
              <Col sm="4"></Col>
              <Col sm="4"></Col>
              <Col sm="4">
                <StdButton toggleNewOrderPopup={this.props.toggleNewOrderPopup} handleOnClick={this.saveOrder} marginTop="58px" width="200px" height="50px">
                  Save Order
                </StdButton>
                <StdButton handleOnClick={this.props.toggleNewOrderPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton handleOnClick={this.prevPage} width="100px" height="50px">
                  Previous
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* Bike Info */}
        {this.state.showBikeInfo && (
          <PopupWindow height="450px" width="400px">
            <Container>
              <Row>
                <CardArea width="100%" height="100%">
                  {bikeInfoComponent}
                </CardArea>
              </Row>
              <Row>
                <StdButton marginTop="10px" handleOnClick={this.toggleBikeInfo}>
                  Close
                </StdButton>
              </Row>
            </Container>
          </PopupWindow>
        )}
      </div>
    );
  }

  mounted() {
    // Retrieves all equipment:
    orderService.getEquipment(item => {
      this.setState({
        equipment: item
      });
    });

    // Retrieves existing customers:
    orderService.getCustomers(item => {
      this.setState({
        customers: item
      });
    });

    // Retrieves location of user:
    orderService.getLocation(item => {
      this.setState({
        location: item
      })
    })
  }
}

export default NewOrder;