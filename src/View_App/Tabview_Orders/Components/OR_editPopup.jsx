import React from 'react';
import { Component } from 'react-simplified';
import date from 'date-and-time';
// Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
// Components
import CardArea from '../../../StandardComponents/stdCardArea';
import StdButton from '../../../StandardComponents/stdButton';
import StdCard from '../../../StandardComponents/stdCard';
import PopupWindow from '../../../StandardComponents/stdComponents';
// Queries
import { orderService } from '../../../Services_orders/service_orderQueries';
import { dbService } from '../../../Services_MYSQL/service_mysqlQueries';

class EditOrderPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderID: this.props.orderID,
      page: 1,
      order: [],
      location: '',
      showSavePopup: false,

      // Date:
      from: '',
      to: '',

      // Bikes:
      removeBike: [],
      bikes: [],
      list: [],
      addedBikeIDs: [],
      showBikeInfo: false,

      // Equipment:
      equipment: [],
      addEq: [],
      addedEquip: [],

      // Customer:
      fullName: '',
      dob: '',
      phone: '',
      email: '',
      personId: '',
      customerID: [],
      comment: ''
    };
  }

  // Adds equipment to order:
  addEquip = id => {
    orderService.selectEquipment(id, item => {
      this.setState({
        addEq: item
      });

      this.setState(state => {
        const addedEquip = state.addedEquip.concat(state.addEq);

        return {
          addedEquip,
          addEq: []
        };
      });
    });
  };

  // Removes equipment from order:
  removeEquip = index => {
    this.setState(state => {
      const addedEquip = state.addedEquip.filter(item => this.state.addedEquip.indexOf(item) !== index);

      return {
        addedEquip
      };
    });
  };

  // Next Page:
  nextPage = () => {
    let prevState = this.state.page;
    this.setState({
      page: prevState + 1
    });
  };

  // Previous Page:
  prevPage = () => {
    let prevState = this.state.page;
    this.setState({
      page: prevState - 1
    });
  };

  // Toggle "Save"-popup:
  toggleSavePopup = () => {
    this.setState({
      showSavePopup: !this.state.showSavePopup
    });
  };

  // Adds the correct values to state when the user inserts something into the inputs:
  setValue = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  // Retrieves the customer information and adds it to state:
  getCustomer = () => {
    orderService.selectCustomer(this.state.customerID, customer => {
      this.setState({
        fullName: customer[0].name,
        dob: customer[0].date_ob,
        phone: customer[0].cell_num,
        email: customer[0].email,
        personId: customer[0].person_id
      });
    });
  };

  // Retrieves all the bikes that is in the order:
  getBikeOrder = () => {
    orderService.getBikeOrder(this.state.orderID, bikes => {
      this.setState(
        {
          addedBikeIDs: bikes
        },
        this.getBikes
      );
    });
  };

  // Splits selected and none-selected bikes into to states:
  getBikes = () => {
    let selectedBikes = this.state.addedBikeIDs.map(item => {
      return item.bike_id;
    });

    // Retrieves available bikes for chosen timeperiod:
    orderService.getAvailableBikes(this.state.location[0].adress, this.state.from, this.state.to, item => {
      // Creates an array with all bikes that are not in the order:
      let noneSelectedBikeList = item.filter(item => {
        if (selectedBikes.some(bike => bike === item.bike_id) === false) {
          return item;
        }
      });

      // Sets state of bikes to that array:
      this.setState({
        bikes: noneSelectedBikeList
      });
    });

    // Retrieves all bikes:
    dbService.getBikes(item => {
      // Creates an array with all the bikes that are in the order:
      let selectedBikeList = item.filter(item => {
        if (selectedBikes.some(bike => bike === item.bike_id)) {
          return item;
        }
      });

      // Sets state of bikes to that array:
      this.setState({
        list: selectedBikeList
      });
    });
  };

  // Adds bike to list:
  addBike = id => {
    this.setState(state => {
      const list = state.list.concat(this.state.bikes.filter(item => item.bike_id === id));
      const bikes = state.bikes.filter(bike => bike.bike_id !== id);

      return {
        list,
        bikes
      };
    });
  };

  // Removes bike from list:
  removeBike = id => {
    this.setState(state => {
      const list = state.list.filter(bike => bike.bike_id !== id);
      const removeBike = state.list.filter(bike => bike.bike_id === id);

      return {
        list,
        removeBike
      };
    });
    this.updateBike();
  };

  // Updates the bike-component when a bike is removed from the order:
  updateBike = () => {
    this.setState(state => {
      const newBikes = state.bikes.concat(state.removeBike);
      const bikes = newBikes.sort((a, b) => {
        return a.bike_id - b.bike_id;
      });

      return {
        bikes,
        removeBike: []
      };
    });
  };

  // Toggle bike info:
  toggleBikeInfo = id => {
    this.setState({
      showBikeInfo: !this.state.showBikeInfo,
      showBikeInfoID: id
    });
  };

  // Adds ordered equipment to state:
  getEquipment = () => {
    orderService.selectOrderEquipment(this.state.orderID, item => {
      for (let i = 0; i < item.length; i++) {
        let amount = item[i].amount;

        for (let j = 0; j < amount; j++) {
          let id = item[i].eq_id;
          this.addEquip(id);
        }
      }
    });
  };

  // Saves new order:
  saveOrder = () => {
    orderService.updateOrder(this.state.orderID, this.state.comment, this.state.from, this.state.to);
    orderService.deleteBikeOrder(this.state.orderID);
    orderService.deleteEquipmentOrder(this.state.orderID);

    setTimeout(() => {
      this.finishOrder();
    }, 1000);
  };

  // Counts how many of each type of equipment that is selected:
  countEq = () => {
    // Counted equipment:
    let eqArray = [];
    // Unique IDs:
    let ids = [];
    // Count of each unique equipment:
    let counts = [];
    let count = 0;

    // Sorts addedEquip by id:
    this.setState(() => {
      const addedEquip = this.state.addedEquip.sort((a, b) => {
        if (a.eq_id !== b.eq_id) {
          return a.eq_id - b.eq_id;
        }
        if (a.name === b.name) {
          return 0;
        }
      });
      return addedEquip;
    });

    for (let i = 0; i < this.state.addedEquip.length; i++) {
      // The id exists inn eqArray:
      if (eqArray.some(x => x.eq_id === this.state.addedEquip[i].eq_id)) {
        count = count + 1;
        eqArray.push(this.state.addedEquip[i]);
        // Push the count if its the last item:
        if (i === this.state.addedEquip.length - 1) {
          counts.push(count);
        }
        // The id does not exist in eqArray:
      } else {
        ids.push(this.state.addedEquip[i].eq_id);
        eqArray.push(this.state.addedEquip[i]);
        if (count === 0) {
          count = 1;
        } else {
          counts.push(count);
          count = 1;
          // Push the count if its the last item:
          if (i === this.state.addedEquip.length - 1) {
            counts.push(count);
          }
        }
      }
    }
    // Does a query for each equipment with its unique amount:
    for (let i = 0; i < ids.length; i++) {
      orderService.orderEquipment(ids[i], this.state.orderID, counts[i]);
    }
  };

  // Maked sure the order is connected to the correct bikes and makes a call for countEq():
  finishOrder = () => {
    this.countEq();
    // Creates a bike_order item for each bike in the order
    for (let i = 0; i < this.state.list.length; i++) {
      orderService.orderBike(this.state.list[i].bike_id, this.state.orderID);
    }
    this.props.toggleEditPopup();
  };

  render() {
    // Sorts bikes after name:
    const sortedBikes = this.state.bikes.sort((a, b) => {
      if (a.make !== b.make) {
        if (a.make < b.make) {
          return -1;
        }
        if (a.make > b.make) {
          return 1;
        }
        return 0;
      } else {
        if (a.name < b.name) {
          return -1;
        }
        if (a.name > b.name) {
          return 1;
        }
        return 0;
      }
    });

    // Component of bikes:
    const bikesComponent = sortedBikes.map(item => (
      <StdCard margin="10px" key={item.bike_id}>
        <Container>
          <Row>
            <Col sm="5">
              <h3>
                {item.make} {item.name} <br />
              </h3>
              Size: {item.size}
            </Col>
            <Col sm="2">
              <StdButton
                marginTop="25px"
                width="40px"
                height="40px"
                handleOnClick={() => {
                  this.toggleBikeInfo(item.bike_id);
                }}
              >
                !
              </StdButton>
            </Col>
            <Col sm="2">
              <StdButton
                marginTop="25px"
                width="100px"
                height="40px"
                handleOnClick={() => {
                  this.addBike(item.bike_id);
                }}
              >
                Add
              </StdButton>
            </Col>
          </Row>
        </Container>
      </StdCard>
    ));

    // Component of bikes added to the order:
    const addedBikesComponent = this.state.list.map(item => (
      <StdCard margin="10px" key={item.bike_id}>
        <Container>
          <Row>
            <Col sm="8">
              <h3>
                {item.make} {item.name}
              </h3>
              <a>Size: {item.size}</a> <br />
              <a>Bike ID: {item.bike_id}</a> <br />
              <a>Chassie nr.: {item.chassienum}</a>
            </Col>
            <Col sm="4">
              <StdButton
                marginTop="13px"
                width="40px"
                height="40px"
                handleOnClick={() => {
                  this.removeBike(item.bike_id);
                }}
                id={item.bike_id}
              >
                X
              </StdButton>
            </Col>
          </Row>
        </Container>
      </StdCard>
    ));

    // Component of equipment:
    const equipComponent = this.state.equipment.map(item => (
      <StdCard margin="10px" key={item.eq_id}>
        <Container>
          <Row>
            <Col sm="8">
              <h3>
                {item.name} <br />
              </h3>
              Amount: {item.amount}
            </Col>
            <Col sm="4">
              <StdButton
                height="50px"
                width="80px"
                marginTop="10px"
                handleOnClick={() => {
                  this.addEquip(item.eq_id);
                }}
              >
                Add
              </StdButton>
            </Col>
          </Row>
        </Container>
      </StdCard>
    ));

    // Component of equipment added to the order:
    const addedEquipComponent = this.state.addedEquip.map(item => (
      <StdCard margin="10px" key={this.state.addedEquip.indexOf(item)}>
        <Container>
          <Row>
            <Col sm="8">
              <h3>{item.name}</h3>
            </Col>
            <Col sm="4">
              <StdButton
                height="40px"
                width="40px"
                marginTop="13px"
                handleOnClick={() => {
                  this.removeEquip(this.state.addedEquip.indexOf(item));
                }}
              >
                X
              </StdButton>
            </Col>
          </Row>
        </Container>
      </StdCard>
    ));

    // Component of info about chosen bike:
    const bikeInfoComponent = this.state.bikes
      .filter(x => x.bike_id === this.state.showBikeInfoID)
      .map(item => (
        <StdCard margin="10px" key={item.bike_id}>
          <Container>
            <Row>
              <h3>
                {item.make} {item.name}
              </h3>
            </Row>
            <Row>
              {item.bike_info} <br /> <br />
            </Row>
            <Row>
              <h5>
                User level: {item.user_level}
                <br />
                Chassie number: {item.chassienum} <br />
                Model: {item.model}
              </h5>
            </Row>
          </Container>
        </StdCard>
      ));

    return (
      <React.Fragment>
        {/* "Edit bikes"-page: */}
        {this.state.page === 1 && (
          <Container>
            <br /> <br />
            <Row>
              <Col xs="6">
                <CardArea height="400px" width="100%">
                  {bikesComponent}
                </CardArea>
              </Col>
              <Col xs="6">
                <CardArea height="300px" width="100%">
                  {addedBikesComponent}
                </CardArea>
                <Form.Group>
                  <a style={{ color: 'white' }}>Order Comment:</a>
                  <Form.Control
                    as="textarea"
                    rows="1"
                    name="comment"
                    value={this.state.comment}
                    onChange={this.setValue}
                  />
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col sm="6">
                <StdCard>
                  <Card.Body>
                    <Row>
                      <Col sm="3">
                        <a>From:</a> <br /> <br />
                        <a>To:</a>
                      </Col>
                      <Col sm="9">
                        <Form>
                          <Form.Group>
                            <Form.Control type="date" name="from" onChange={this.setValue} value={this.state.from} />
                          </Form.Group>
                          <Form.Group>
                            <Form.Control type="date" name="to" onChange={this.setValue} value={this.state.to} />
                          </Form.Group>
                        </Form>
                      </Col>
                    </Row>
                  </Card.Body>
                </StdCard>
              </Col>
              <Col sm="2" />
              <Col sm="4">
                <StdButton handleOnClick={this.props.toggleEditPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton handleOnClick={this.nextPage} width="100px" height="50px">
                  Next
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* "Edit equipment"-page: */}
        {this.state.page === 2 && (
          <Container>
            <br /> <br />
            <Row>
              <Col xs="6">
                <CardArea height="400px" width="100%">
                  {equipComponent}
                </CardArea>
              </Col>
              <Col xs="6">
                <CardArea height="300px" width="100%">
                  {addedEquipComponent}
                </CardArea>
              </Col>
            </Row>
            <Row>
              <Col sm="8" />
              <Col sm="4">
                <StdButton handleOnClick={this.props.toggleEditPopup} width="100px" height="50px">
                  Cancel
                </StdButton>
                <StdButton handleOnClick={this.prevPage} width="100px" height="50px">
                  Previous
                </StdButton>{' '}
                <br />
                <StdButton handleOnClick={this.toggleSavePopup} width="200px" height="50px">
                  Save
                </StdButton>
              </Col>
            </Row>
          </Container>
        )}
        {/* "Bike Info"-popup: */}
        {this.state.showBikeInfo && (
          <PopupWindow height="350px" width="400px">
            <Container>
              <Row>
                <CardArea width="100%" height="250px">
                  {bikeInfoComponent}
                </CardArea>
              </Row>
              <Row>
                <StdButton marginTop="10px" handleOnClick={this.toggleBikeInfo}>
                  Close
                </StdButton>
              </Row>
            </Container>
          </PopupWindow>
        )}
        {/* "Save"-popup: */}
        {this.state.showSavePopup && (
          <PopupWindow height="150px" width="400px">
            <Container>
              <Row>
                <div style={{ color: 'white', textAlign: 'center' }}>
                  <Col sm="12">
                    <h3>Are you sure you want to save changes?</h3>
                  </Col>
                </div>
              </Row>
              <Row>
                <Col sm="6">
                  <div style={{ textAlign: 'center' }}>
                    <StdButton
                      width="100px"
                      marginTop="5px"
                      backgroundColor="red"
                      handleOnClick={() => {
                        this.saveOrder(), this.toggleSavePopup();
                      }}
                    >
                      Yes
                    </StdButton>
                  </div>
                </Col>
                <Col sm="6">
                  <div style={{ textAlign: 'center' }}>
                    <StdButton width="100px" marginTop="5px" handleOnClick={this.toggleSavePopup}>
                      No
                    </StdButton>
                  </div>
                </Col>
              </Row>
            </Container>
          </PopupWindow>
        )}
      </React.Fragment>
    );
  }

  mounted() {
    // Retrieves all order info:
    orderService.getOrder(this.state.orderID, item => {
      this.setState(
        {
          order: item[0],
          customerID: item[0].customer_id,
          from: date.format(new Date(item[0].from_date), 'YYYY-MM-DD'),
          to: date.format(new Date(item[0].to_date), 'YYYY-MM-DD'),
          comment: item[0].order_comment
        },
        this.getCustomer,
        this.getBikeOrder()
      );
    });

    // Retrieves all equipment:
    orderService.getEquipment(item => {
      this.setState(
        {
          equipment: item
        },
        this.getEquipment
      );
    });

    // Retrieves location of user:
    orderService.getLocation(item => {
      this.setState({
        location: item
      });
    });
  }
}

export default EditOrderPopup;
