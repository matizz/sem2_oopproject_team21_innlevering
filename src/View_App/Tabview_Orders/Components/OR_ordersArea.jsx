import React from 'react';
import { Component } from 'react-simplified';
import date from 'date-and-time';
// Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
// Components
import StdCards from '../../../StandardComponents/stdCard';
import CardArea from '../../../StandardComponents/stdCardArea';
// Queries
import { orderService } from '../../../Services_orders/service_orderQueries';

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      customerNames: [],
      customerIDs: []
    };
  }

  render() {
    // Sorts orders after from-date:
    const sortedOrders = this.props.orders.sort((a, b) => {
      if (a.from_date !== b.from_date) {
        return a.from_date - b.from_date;
      } else {
        return 0;
      }
    });

    // An array component of all the orders:
    const orderComponent = sortedOrders.map(item => (
      <StdCards
        selectable="true"
        margin="15px"
        key={item.order_id}
        onClick={() => {
          this.props.selectOrder(item.order_id);
        }}
      >
        <Card.Header>
          <h3>Order ID: {item.order_id}</h3>
        </Card.Header>
        <Card.Body>
          <Container>
            <Row>
              <Col sm="4">
                <a>Customer:</a> <br />
                <a>From:</a> <br />
                <a>To:</a> <br />
                <a>Status:</a> <br />
                <a>Order ID:</a> <br />
                <a>Comment:</a>
              </Col>
              <Col sm="8">
                <a>{this.state.customerNames[this.state.customerIDs.indexOf(item.customer_id)]}</a> <br />
                <a>{date.format(new Date(item.from_date), 'DD/MM-YYYY')}</a> <br />
                <a>{date.format(new Date(item.to_date), 'DD/MM-YYYY')}</a> <br />
                <a>{item.fulfilled_name}</a> <br />
                <a>{item.order_id}</a> <br />
                <a>{item.order_comment}</a>
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </StdCards>
    ));

    return (
      <CardArea height="80vh" width="100%">
        {orderComponent}
      </CardArea>
    );
  }

  mounted() {
    orderService.getCustomers(customer => {
      let customerNames = [];
      let customerIDs = [];
      for (let i = 0; i < customer.length; i++) {
        customerNames.push(customer[i].name);
        customerIDs.push(customer[i].customer_id);
      }
      this.setState({
        customerNames: customerNames,
        customerIDs: customerIDs
      });
    });
  }
}

export default Orders;
