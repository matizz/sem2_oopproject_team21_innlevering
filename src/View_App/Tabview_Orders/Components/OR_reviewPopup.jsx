import React from 'react';
import { Component } from 'react-simplified';
import date from 'date-and-time';
// Bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
// Components
import CardArea from '../../../StandardComponents/stdCardArea';
import PopupWindow from '../../../StandardComponents/stdComponents';
import StdButton from '../../../StandardComponents/stdButton';
import StdCard from '../../../StandardComponents/stdCard';
// Queries
import { dbService } from '../../../Services_MYSQL/service_mysqlQueries';
import { orderService } from '../../../Services_orders/service_orderQueries';

const infoStyle = {
  marginTop: '10px',
  textAlign: 'center',
  height: '50px',
  width: '100%',
  maxHeight: '50vh',
  color: 'white',
  padding: '0'
};

class ReviewAndDeliver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // Popup:
      showPopup: false,

      // Order:
      orderID: this.props.orderID,
      order: [],
      from: '',
      to: '',
      comment: '',

      // Bikes:
      bikeIDs: [],
      list: [],

      // Equipment:
      eqID: [],
      addEq: [],
      addedEquip: [],

      // Customer:
      name: '',
      dob: '',
      customerID: [],
      customer: [],
      phone: '',
      email: '',
      personId: ''
    };
  }

  // Retrieves all customer info:
  getCustomer = id => {
    orderService.selectCustomer(id, item => {
      this.setState({
        customer: item,
        name: item[0].name,
        dob: item[0].date_ob,
        phone: item[0].cell_num,
        email: item[0].email,
        personId: item[0].person_id
      });
    });
  };

  // Retrives the bike IDs of the bikes in the order:
  getBikeID = id => {
    orderService.getBikeOrder(id, item => {
      this.setState({
        bikeIDs: item
      }),
        this.getBikes();
    });
  };

  // Retrieves the bikes that are in the order:
  getBikes = () => {
    let selectedBikes = this.state.bikeIDs.map(item => {
      return item.bike_id;
    });

    // Retrieves all bikes from db:
    dbService.getBikes(item => {
      // Creates an array with all the bikes that are in the order:
      let selectedBikeList = item.filter(item => {
        if (selectedBikes.some(bike => bike === item.bike_id)) {
          return item;
        }
      });

      // Sets state of bikes to that array:
      this.setState({
        list: selectedBikeList
      });
    });
  };

  // Adds ordered equipment to state:
  getEquipment = () => {
    orderService.selectOrderEquipment(this.state.orderID, item => {
      for (let i = 0; i < item.length; i++) {
        let amount = item[i].amount;

        for (let j = 0; j < amount; j++) {
          let id = item[i].eq_id;
          this.addEquip(id);
        }
      }
    });
  };

  // Adds equipment to order:
  addEquip = id => {
    orderService.selectEquipment(id, item => {
      this.setState({
        addEq: item
      });

      this.setState(state => {
        const addedEquip = state.addedEquip.concat(state.addEq);

        return {
          addedEquip,
          addEq: []
        };
      });
    });
  };

  // Sets fulfilled to correct value depending on wether its being returned or delivered:
  deliverOrder = () => {
    if (this.state.order[0].fulfilled === 0) {
      // If the order is being delivered:
      orderService.deliverOrder(1, this.state.orderID);
      orderService.deliverBike(2, this.state.orderID);
    } else if (this.state.order[0].fulfilled === 1) {
      // If the order is being returned:
      orderService.deliverOrder(2, this.state.orderID);
      orderService.deliverBike(1, this.state.orderID);
    }
    this.props.toggleReviewPopup();
  };

  // Toggles popup:
  togglePopup = () => {
    this.setState({
      showPopup: !this.state.showPopup
    });
  };

  render() {
    const bikeComponent = this.state.list.map(item => (
      <StdCard key={item.bike_id} margin="10px">
        <Card.Header>
          <h3>
            {item.make} {item.name}
          </h3>
        </Card.Header>
        <Card.Body>
          <Row>
            <Col sm="4">
              <a>Bike ID:</a> <br />
              <a>Chassie nr.:</a> <br />
              <a>Size:</a>
            </Col>
            <Col sm="8">
              <a>{item.bike_id}</a> <br />
              <a>{item.chassienum}</a> <br />
              <a>{item.size}</a>
            </Col>
          </Row>
        </Card.Body>
      </StdCard>
    ));

    const equipmentComponent = this.state.addedEquip.map(item => (
      <StdCard margin="10px" key={this.state.addedEquip.indexOf(item)}>
        <h3>{item.name}</h3>
      </StdCard>
    ));

    return (
      <Container>
        <Row>
          <Col xs="6">
            <StdCard>
              <Card.Header>
                <h3>Customer Info</h3>
              </Card.Header>
              <Card.Body>
                <Row>
                  <Col>
                    <a>Name: </a> <br /> <br />
                    <a>Date of Birth: </a> <br /> <br />
                    <a>Phone: </a> <br /> <br />
                    <a>Email: </a> <br /> <br />
                    <a>Person ID: </a> <br />
                  </Col>
                  <Col>
                    <a>{this.state.name}</a> <br /> <br />
                    <a>{date.format(new Date(this.state.dob), 'DD/MM-YYYY')}</a> <br /> <br />
                    <a>{this.state.phone}</a> <br /> <br />
                    <a>{this.state.email}</a> <br /> <br />
                    <a>{this.state.personId}</a> <br /> <br />
                  </Col>
                </Row>
              </Card.Body>
            </StdCard>{' '}
            <br />
          </Col>
          <Col xs="6">
            <CardArea width="100%" height="350px">
              <div style={infoStyle}>
                <h3>Bikes:</h3>
              </div>
              {bikeComponent}
              <div style={infoStyle}>
                <h3>Equipment:</h3>
              </div>
              {equipmentComponent}
            </CardArea>
          </Col>
        </Row>
        <Row>
          <Col xs="6">
            <StdCard>
              <Card.Header>
                <h3>Order Info</h3>
              </Card.Header>
              <Card.Body>
                <Row>
                  <Col>
                    <a>From: </a> <br /> <br />
                    <a>To: </a>
                  </Col>
                  <Col>
                    <a>{date.format(new Date(this.state.from), 'DD/MM-YYYY')}</a> <br /> <br />
                    <a>{date.format(new Date(this.state.to), 'DD/MM-YYYY')}</a> <br />
                  </Col>
                </Row>
              </Card.Body>
            </StdCard>
          </Col>
          <Col xs="6">
            <StdCard>
              <Card.Header>
                <h3>Order Comment</h3>
              </Card.Header>
              <Card.Body>
                <a>{this.state.comment}</a>
              </Card.Body>
            </StdCard>
          </Col>
        </Row>
        <Row>
          <Col sm="4" />
          <Col sm="4" />
          <Col sm="4">
            <StdButton handleOnClick={this.togglePopup}>Deliver/Return</StdButton>
            <StdButton handleOnClick={this.props.toggleReviewPopup}>Close</StdButton>
          </Col>
        </Row>
        {/* "Save"-popup: */}
        {this.state.showPopup && (
          <PopupWindow height="150px" width="400px">
            <Container>
              <Row>
                <div style={{ color: 'white', textAlign: 'center' }}>
                  <Col sm="12">
                    <h3>Are you sure you want to deliver/return order?</h3>
                  </Col>
                </div>
              </Row>
              <Row>
                <Col sm="6">
                  <div style={{ textAlign: 'center' }}>
                    <StdButton
                      width="100px"
                      marginTop="5px"
                      backgroundColor="red"
                      handleOnClick={() => {
                        this.deliverOrder(), this.togglePopup();
                      }}
                    >
                      Yes
                    </StdButton>
                  </div>
                </Col>
                <Col sm="6">
                  <div style={{ textAlign: 'center' }}>
                    <StdButton width="100px" marginTop="5px" handleOnClick={this.togglePopup}>
                      No
                    </StdButton>
                  </div>
                </Col>
              </Row>
            </Container>
          </PopupWindow>
        )}
      </Container>
    );
  }

  mounted() {
    orderService.getOrder(this.state.orderID, order => {
      this.setState(
        {
          order: order,
          customerID: order[0].customer_id,
          from: order[0].from_date,
          to: order[0].to_date,
          comment: order[0].order_comment
        },
        this.getCustomer(order[0].customer_id),
        this.getBikeID(order[0].order_id),
        this.getEquipment()
      );
    });
  }
}

export default ReviewAndDeliver;
