import React from 'react';
import { Component } from 'react-simplified';
// Bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
// Components
import StdButton from '../../../StandardComponents/stdButton';

const chooseButtons = {
  borderStyle: 'solid',
  border: 'solid rgba(0,0,0,0.3) 2px',
  margin: 'auto',
  textAlign: 'center',
  padding: '0',
  marginTop: '15px',
  width: '100%'
};

const newOrderStyle = {
  margin: 'auto',
  textAlign: 'center',
  padding: '0'
};

const bikeInfoAreaStyle = {
  marginTop: '10px',
  textAlign: 'center',
  height: '50px',
  width: '100%',
  maxHeight: '50vh',
  color: 'white',
  padding: '0'
};

class Buttons extends Component {
  render() {
    return (
      <Container>
        <Row>
          <div style={chooseButtons}>
            <div style={bikeInfoAreaStyle}>
              <h3>Orders</h3>
            </div>
            <StdButton
              handleOnClick={this.props.toggleEditPopup}
              margin="auto"
              width="300px"
              marginTop="20px"
              height="60px"
            >
              Edit
            </StdButton>
            <br />
            <StdButton handleOnClick={this.props.toggleReviewPopup} width="300px" height="60px">
              Review and Deliver/Return
            </StdButton>
            <br />
            <StdButton handleOnClick={this.props.deleteOrder} backgroundColor="red" width="300px" height="60px">
              Delete
            </StdButton>
          </div>
        </Row>
        <Row>
          <div style={newOrderStyle}>
            <StdButton handleOnClick={this.props.toggleNewOrderPopup} marginTop="20px" width="300px" height="120px">
              New Order
            </StdButton>
          </div>
        </Row>
      </Container>
    );
  }
}

export default Buttons;
