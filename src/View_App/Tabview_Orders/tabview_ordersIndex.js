import React from 'react';
import { Component } from 'react-simplified';
// Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
// Components
import Orders from './Components/OR_ordersArea';
import Buttons from './Components/OR_buttonsArea';
import Search from '../../StandardComponents/stdSearchArea';
import PopupWindow from '../../StandardComponents/stdComponents';
import NewOrderPop from './Components/OR_newOrderPopup';
import Edit from './Components/OR_editPopup';
import Review from './Components/OR_reviewPopup';
import StdButton from '../../StandardComponents/stdButton';
// Queries
import { orderService } from '../../Services_orders/service_orderQueries';
import { Search as SearchService } from './../../Services_Search/services_search';


const deleteStyle = {
    textAlign: "center"
}

class ordersIndex extends Component {
    constructor() {
        super();
        this.state = {
            showNewOrderPopup: false,
            showEditPopup: false,
            showReviewPopup: false,
            showDeletePopup: false,
            orderID: [],
            allOrders: [],
            filteredOrders: []
        };
    }

    // Handles search:
    handleOnSearch = searchTerm => {
        const matches = SearchService.orderSearch(searchTerm, this.state.allOrders);
        this.setState({ filteredOrders: matches });
    }

    // Handles selecting an order:
    selectOrder = id => {
        this.setState({
            orderID: id
        })
    }

    // Toggles the "New Order"-popup:
    toggleNewOrderPopup = () => {
        this.setState({
            showNewOrderPopup: !this.state.showNewOrderPopup
        });
    };

    // Toggles the "Edit"-popup:
    toggleEditPopup = () => {
        if(this.state.orderID.length === 0 ) {
            alert('Please select an order')
        } else {
            this.setState({
                showEditPopup: !this.state.showEditPopup
            })
        }
    }

    // Toggles the "Review and deliver"-popup:
    toggleReviewPopup = () => {
        this.setState({
            showReviewPopup: !this.state.showReviewPopup
        })
    }

    // Toggle "Delete"-popup:
    toggleDeletePopup = () => {
        this.setState({
            showDeletePopup: !this.state.showDeletePopup
        })
    }

    // Deletes selected order:
    deleteOrder = () => {
        orderService.deleteOrder(this.state.orderID)
        this.componentWillMount()
    }

    render() {
        return (
            <React.Fragment>
                <Container fluid={true}>
                    <Row>
                        <Col>
                            <Search onSubmit={this.handleOnSearch} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="6">
                            <Orders selectOrder={id => this.selectOrder(id)} orders={this.state.filteredOrders} />
                        </Col>
                        <Col sm="6">
                            <Buttons deleteOrder={this.toggleDeletePopup} toggleReviewPopup={this.toggleReviewPopup} toggleEditPopup={this.toggleEditPopup} toggleNewOrderPopup={this.toggleNewOrderPopup} />
                        </Col>
                    </Row>
                </Container>
                {/* "New Order"-popup: */}
                {this.state.showNewOrderPopup && (
                    <PopupWindow height="650px" width="900px">
                        <br />
                        <br />
                        <NewOrderPop toggleNewOrderPopup={this.toggleNewOrderPopup} />
                    </PopupWindow>
                )}
                {/* "Edit"-popup: */}
                {this.state.showEditPopup && (
                    <PopupWindow height="650px" width="900px">
                        <Edit orderID={this.state.orderID} toggleEditPopup={this.toggleEditPopup} />
                    </PopupWindow>
                )}
                {/* "Review and deliver"-popup: */}
                {this.state.showReviewPopup && (
                    <PopupWindow height="650px" width="900px">
                        <br />
                        <Review orderID={this.state.orderID} toggleReviewPopup={this.toggleReviewPopup} />
                    </PopupWindow>
                )}
                {/* "Delete Order"-popup: */}
                {this.state.showDeletePopup && (
                    <PopupWindow height="150px" width="400px">
                        <Container>
                            <Row>
                                <div style={{color:'white', textAlign: 'center'}}>
                                    <Col sm="12"><h3>Are you sure you want to delete this order?</h3></Col> 
                                </div>
                            </Row>
                            <Row>
                                <Col sm="6">
                                    <div style={deleteStyle}>
                                        <StdButton width="100px" marginTop="5px" backgroundColor="red" handleOnClick={()=>{this.deleteOrder(), this.toggleDeletePopup()}}>
                                            Yes
                                        </StdButton>
                                    </div>
                                </Col>
                                <Col sm="6">
                                    <div style={deleteStyle}>
                                        <StdButton width="100px" marginTop="5px" handleOnClick={this.toggleDeletePopup}>
                                            No
                                        </StdButton>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </PopupWindow>
                )}
            </React.Fragment>
        );
    }

    componentWillMount() {
        orderService.getOrders(orders => {
            this.setState({
                allOrders: orders,
            }, () => this.handleOnSearch(""));

        });
    }
}

export default ordersIndex;