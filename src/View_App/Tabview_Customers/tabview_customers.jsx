import React from 'react';
import { Component } from 'react-simplified';
//imports db-queries
import { dbServiceCustomers } from '../../Services_Customers/service_Customers';
import { Search as SearchService } from './../../Services_Search/services_search';
//imports standardcomponents
import StdCard from '../../StandardComponents/stdCard';
import CardArea from '../../StandardComponents/stdCardArea';
import StdButton from '../../StandardComponents/stdButton';
import Search from '../../StandardComponents/stdSearchArea';
import PopupWindow from '../../StandardComponents/stdComponents';
//imports bootstrap
import Form from 'react-bootstrap/form';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
//import a package to format date
import moment from 'moment';

import date from 'date-and-time'; 

//global variable with the customer id
let thisCustomerId;

class Customers extends Component {
  //variables to store query data
  allCustomers = [];
  filteredCustomers = [];
  //variable to handle invisible button
  selectedCustomerId = -1;

  //toggles for showing popups
  showPopupNewCustomer = false;
  showPopupEditCustomer = false;

  //function to handle searches
  handleOnSearch = searchTerm => {
    let matches = [];
    matches = SearchService.customerSearch(searchTerm, this.allCustomers);

    this.filteredCustomers = matches;
  };
  //handlers for showing popups
  togglePopupNewCustomer = () => {
    this.showPopupNewCustomer = !this.showPopupNewCustomer;
  };

  togglePopupEditCustomer = () => {
    this.showPopupEditCustomer = !this.showPopupEditCustomer;
  };

  onConfirmedNewCustomer = () => {
    this.togglePopupNewCustomer();
  };

  onConfirmedEditCustomer = () => {
    this.togglePopupEditCustomer();
  };

  //function to fetch all the customers from the database
  getCustomers = () => {
    dbServiceCustomers.getCustomers(customers => {
      this.allCustomers = customers;
      //empties the search when the page loads
      this.handleOnSearch('');
    });
  };

  //gets the customer id and stores it for later use
  setSelectedCustomerInfo = id => {
    thisCustomerId = id;
    this.selectedCustomerId = id;
  };

  render() {
    const infoDiv = {
      borderStyle: 'solid',
      border: 'solid rgba(0,0,0,0.3) 2px',
      margin: 'auto',
      textAlign: 'center',
      padding: '0',
      marginTop: '15px',
      width: '100%',
      color: 'white',
      marginLeft: '15px',
      marginRight: '15px'
    };

    const bikeInfoAreaStyle = {
      marginTop: '10px',
      textAlign: 'center',
      height: '50px',
      width: '100%',
      maxHeight: '50vh',
      padding: '0'
    };

    //creates customer-cards
    let customerCards;
    customerCards = this.filteredCustomers.map(item => (
      <StdCard
        key={item.customer_id}
        onClick={() => {
          this.setSelectedCustomerInfo(item.customer_id);
        }}
        selectable={true}
        margin="15px"
      >
        <Card.Header>
          <h1>{item.name}</h1>
        </Card.Header>
        <Card.Body>
          <Container fluid={true}>
            <Row>
              <Col sm={4}>
                <a>Email:</a>
                <br />
                <a>Phone:</a>
                <br />
                <a>Date of Birth:</a>
              </Col>
              <Col sm={8}>
                {item.email}
                <br />
                {item.cell_num}
                <br />
                {date.format(new Date(item.date_ob), 'DD/MM-YYYY')}
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </StdCard>
    ));

    //variables for showing popups
    let editCustomerButton;
    let newBikePopup;
    let editBikePopup;

    //show only the edit button if customer is selected. Its not possible to click this button if no costumer is chosen.
    if (this.selectedCustomerId != -1) {
      editCustomerButton = (
        <StdButton handleOnClick={this.togglePopupEditCustomer} height="40px" width="250px" marginTop="20px">
          Edit customer
        </StdButton>
      );
    } else editCustomerButton = null;

    //selective rendering of newBikePopup
    if (this.showPopupNewCustomer) {
      newBikePopup = (
        <NewCustomerPopup
          onAddNewCustomer={this.mounted}
          onCancel={this.togglePopupNewCustomer}
          onConfirmed={this.onConfirmedNewCustomer}
        />
      );
    } else newBikePopup = null;

    //selective rendering of editBikePopup
    if (this.showPopupEditCustomer) {
      editBikePopup = (
        <EditCustomer
          onUpdateCustomer={this.mounted}
          onCancel={this.togglePopupEditCustomer}
          onConfirmed={this.onConfirmedEditCustomer}
        />
      );
    } else editBikePopup = null;

    //returns the entire page
    return (
      <Container fluid={true}>
        <Row>
          <Col>
            <Search onSubmit={this.handleOnSearch} />
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <CardArea width="100%" height="80vh">
              {customerCards}
            </CardArea>
          </Col>
          <Col md={6}>
            <Row>
              <div style={infoDiv}>
                <div>
                  <h3 style={bikeInfoAreaStyle}>Customer details</h3>
                </div>
              </div>
            </Row>
            <Row>
              <Col>
                <ButtonGroup>
                  <StdButton handleOnClick={this.togglePopupNewCustomer} height="40px" width="250px" marginTop="20px">
                    Add new customer
                  </StdButton>
                  <br />
                  {editCustomerButton}
                </ButtonGroup>
              </Col>
            </Row>
          </Col>
        </Row>
        {newBikePopup}
        {editBikePopup}
      </Container>
    );
  }

  mounted() {
    this.getCustomers();
  }
}

//class for styling buttons
class ButtonGroup extends Component {
  render() {
    let buttonGroupStyle = {
      textAlign: 'center'
    };
    return <div style={buttonGroupStyle}>{this.props.children}</div>;
  }
}

//class for new customer popup
class NewCustomerPopup extends Component {
  //variables from input fields
  firstName = '';
  lastName = '';
  date = '';
  cellphone = '';
  email = '';
  personId = '';

  //array with the new customer
  newCustomerArray = [];

  //happends when you click the ok button
  handleOnClickOk = () => {
    this.addNewCustomer();
    this.props.onConfirmed();
    this.props.onAddNewCustomer();
  };

  //happends when you click the cancel button
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  //how to add a costumer into the db
  addNewCustomer = () => {
    dbServiceCustomers.addNewCustomer(
      this.firstName,
      this.lastName,
      this.email,
      this.cellphone,
      this.date,
      this.personId,
      newCustomerArray => {
        this.newCustomerArray = newCustomerArray;
      }
    );
  };
  render() {
    return (
      //how the popup looks, shows what you need to fill in for the db to correctly apply a new costumer to the system.
      <div style={{ color: 'white' }}>
        <PopupWindow height="650px" width="450px">
          <h4 style={{ marginTop: '10px', marginLeft: '10px' }}>New Customer</h4>
          <Form style={{ marginLeft: '30px', marginRight: '30px', position: 'absolute' }}>
            <div style={{ marginLeft: '50px' }}>
              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.firstName = event.target.value)}>
                  <Form.Label>First name:</Form.Label>
                  <Form.Control type="text" placeholder="John" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.lastName = event.target.value)}>
                  <Form.Label>Last name:</Form.Label>
                  <Form.Control type="text" placeholder="Doe" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.date = event.target.value)}>
                  <Form.Label>Date of Birth:</Form.Label>
                  <Form.Control type="date" placeholder="date placeholder" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.cellphone = event.target.value)}>
                  <Form.Label>Phone:</Form.Label>
                  <Form.Control type="text" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.email = event.target.value)}>
                  <Form.Label>Email:</Form.Label>
                  <Form.Control type="text" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.personId = event.target.value)}>
                  <Form.Label>Person ID:</Form.Label>
                  <Form.Control type="text" placeholder="ex 109450010810" />
                </Form.Group>
              </Form.Row>
            </div>
          </Form>

          <StdButton
            handleOnClick={this.handleOnClickCancel}
            width="100px"
            height="50px"
            float="right"
            marginRight="10px"
            marginTop="530px"
          >
            Cancel
          </StdButton>

          <StdButton
            width="100px"
            height="50px"
            handleOnClick={this.handleOnClickOk}
            float="right"
            marginRight="10px"
            marginTop="530px"
            postion="absolute"
          >
            OK
          </StdButton>
        </PopupWindow>
      </div>
    );
  }
}

//class for edit customer popup
class EditCustomer extends Component {
  //variables from the input fields
  name = '';
  date = '';
  cellphone = '';
  email = '';
  personId = '';

  //variables to store arrays from queries
  newCustomerArray = [];
  updatedCustomerArray = [];

  //functions that runs when ok is clicked
  handleOnClickOk = () => {
    this.updateCustomer();
    this.props.onConfirmed();
  };

  //functions that runs when cancel is clicked
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  //the function to update a customer
  updateCustomer = () => {
    //the query with all the input values
    dbServiceCustomers.updateCustomer(
      thisCustomerId,
      this.name,
      this.email,
      this.cellphone,
      this.date,
      this.personId,
      updatedCustomerArray => {
        this.updatedCustomerArray = updatedCustomerArray;
        //calls the prop to refresh customer-list
        this.props.onUpdateCustomer();
      }
    );
  };
  render() {
    return (
      //the edit-popup, already filled in with current costumer information. Will be changed in db when success.
      <div style={{ color: 'white' }}>
        <PopupWindow height="650px" width="450px">
          <h4 style={{ marginTop: '10px', marginLeft: '10px' }}>Edit Customer</h4>
          <Form style={{ marginLeft: '30px', marginRight: '30px', position: 'absolute' }}>
            <div style={{ marginLeft: '50px' }}>
              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.name = event.target.value)}>
                  <Form.Label>Name:</Form.Label>
                  <Form.Control defaultValue={this.name} type="text" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.date = event.target.value)}>
                  <Form.Label>Date of Birth:</Form.Label>
                  <Form.Control type="date" defaultValue={this.date} />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.cellphone = event.target.value)}>
                  <Form.Label>Phone:</Form.Label>
                  <Form.Control type="text" defaultValue={this.cellphone} />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.email = event.target.value)}>
                  <Form.Label>Email:</Form.Label>
                  <Form.Control type="text" defaultValue={this.email} />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.personId = event.target.value)}>
                  <Form.Label>Person ID:</Form.Label>
                  <Form.Control type="text" defaultValue={this.personId} />
                </Form.Group>
              </Form.Row>
            </div>
          </Form>

          <StdButton
            handleOnClick={this.handleOnClickCancel}
            width="100px"
            height="50px"
            float="right"
            marginRight="10px"
            marginTop="530px"
          >
            Cancel
          </StdButton>

          <StdButton
            width="100px"
            height="50px"
            handleOnClick={this.handleOnClickOk}
            float="right"
            marginRight="10px"
            marginTop="530px"
            postion="absolute"
          >
            OK
          </StdButton>
        </PopupWindow>
      </div>
    );
  }

  mounted() {
    //gets data from a customer based on the customer-id stored in the global variable
    dbServiceCustomers.getCustomer(thisCustomerId, student => {
      this.name = student.name;
      //we use a package to format the date so it can be inserted back into the db
      this.date = moment(student.date_ob).format('YYYY-MM-DD');
      this.cellphone = student.cell_num;
      this.email = student.email;
      this.personId = student.person_id;
    });
  }
}

export default Customers;
