import React, { Component } from 'react';
import Bikes from './Components/bike_Bikes';

class BikeIndex extends Component {
  render() {
    return (
      <React.Fragment>
        <div>
          <div>
            <Bikes />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BikeIndex;
