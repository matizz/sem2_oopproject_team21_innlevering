import React from 'react';
import { Component } from 'react-simplified';
//imports databasescript and queries
import { dbService } from '../../../Services_MYSQL/service_mysqlQueries';
import { dbServiceBikes } from '../../../Services_bikes/service_bikeQueries';
//imports standardcomponents
import StdCard from '../../../StandardComponents/stdCard';
import CardArea from '../../../StandardComponents/stdCardArea';
import StdButton from '../../../StandardComponents/stdButton';
import PopupWindow from '../../../StandardComponents/stdComponents';
import Search from '../../../StandardComponents/stdSearchArea';
//imports bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';
import ReactLoading from 'react-loading';
import Form from 'react-bootstrap/Form';
//imports search
import { Search as SearchService } from './../../../Services_Search/services_search';
//global variables
let thisBikeId;
let foreignFilter = [];
let foreignBikeComponent;

//main component of bikes tab
class Bikes extends Component {
  //variables for getting the bikes from the database
  allBikes = [];
  //filtered search
  filteredBikes = [];
  //string to get the info from the bike
  bikeInfo = '';

  //makes it so maintenance and remove buttons is invisible at the start
  selectedBikeID = -1;

  //Flags
  gettingBikes = false;

  //Toggles for showing popups
  showPopupMaintenance = false;
  showPopupNewBike = false;
  showPopupRemoveBike = false;
  showPopupForeignBike = false;
  showPopupForeignBikeConfirmed = false;
  foreignBikeResult = false;
  showPopupConfirmed = false;
  showPopupEditBike = false;

  //Handlers for showing popups
  togglePopupRemoveBike = () => {
    this.showPopupRemoveBike = !this.showPopupRemoveBike;
  };

  togglePopupMaintenance = () => {
    this.showPopupMaintenance = !this.showPopupMaintenance;
  };

  togglePopupNewBike = () => {
    this.showPopupNewBike = !this.showPopupNewBike;
  };

  togglePopupForeignBike = () => {
    this.showPopupForeignBike = !this.showPopupForeignBike;
  };

  togglePopupForeignBikeConfirmed = () => {
    this.showPopupForeignBikeConfirmed = !this.showPopupForeignBikeConfirmed;
  };

  togglePopupConfirmed = () => {
    this.showPopupConfirmed = !this.showPopupConfirmed;
  };

  //shows confirmation boxes
  onConfirmedRemoveBike = () => {
    this.togglePopupRemoveBike();
    this.togglePopupConfirmed();
  };

  onConfirmedMaintenanceBike = () => {
    this.togglePopupMaintenance();
    this.togglePopupConfirmed();
  };

  onConfirmedRegisterBike = () => {
    this.togglePopupNewBike();
    this.togglePopupConfirmed();
  };

  onConfirmedRegisterForeignBike = () => {
    this.togglePopupForeignBike();
    this.togglePopupConfirmed();
  };

  //general handler
  handleOnSerarch = searchTerm => {
    let matches = [];
    matches = SearchService.bikeSearch(searchTerm, this.allBikes);

    this.filteredBikes = matches;
  };

  //get info from the currently selected bike
  setSelectedBikeInfo = (id, info) => {
    thisBikeId = id;
    this.selectedBikeID = id;
    console.log(this.selectedBikeID);
    this.bikeInfo = info;
  };

  //Get all bikes from the db
  getBikes = () => {
    this.gettingBikes = true;
    dbService.getBikes(bikes => {
      this.allBikes = bikes;
      this.gettingBikes = false;

      //search for all bikes. Used to display bikes when db updates.
      this.handleOnSerarch('');
    });
  };

  render() {
    let bikeCards;

    //selective rendering of the bikecards
    if (this.filteredBikes.length > 0)
      bikeCards = this.filteredBikes.map(item => (
        <StdCard
          key={item.bike_id}
          onClick={() => {
            this.setSelectedBikeInfo(item.bike_id, item.bike_info);
          }}
          selectable={true}
          margin="15px"
        >
          <Card.Header>
            <h1>
              {item.make} {item.name}
            </h1>
          </Card.Header>
          <Card.Body>
            <Container fluid={true}>
              <Row>
                <Col sm={4}>
                  <a>ID:</a>
                  <br />
                  <a>Type:</a>
                  <br />
                  <a>Chassie nr:</a>
                  <br />
                  <a>Belongs to:</a>
                  <br />
                  <a>Size:</a>
                  <br />
                  <a>Age:</a>
                  <br />
                  <a>Gender:</a>
                  <br />
                  <a>Status:</a>
                </Col>
                <Col sm={8}>
                  {item.bike_id}
                  <br />
                  {item.bike_type}
                  <br />
                  {item.chassienum}
                  <br />
                  {item.zip_location}
                  <br />
                  {item.size}
                  <br />
                  {item.age}
                  <br />
                  {item.gender_name}
                  <br />
                  {item.status_name}
                </Col>
              </Row>
            </Container>
          </Card.Body>
        </StdCard>
      ));
    else if (this.gettingBikes)
      //show a spinner while the query runs
      bikeCards = (
        <div width="100%" style={{ width: '13.5%', margin: '20vh auto' }}>
          <ReactLoading type="spin" color="black" delay={250} />
        </div>
      );
    //no bikes matching search
    else
      bikeCards = (
        <div style={{ textAlign: 'center', color: 'white', margin: '20vh auto' }}>No bikes mathcing search</div>
      );
    //variables to make the popups visible/invisible
    let bikeSpecificButtons;
    let removeBikePopup;
    let maintenanceBikePopup;
    let registerBikePopup;
    let registerForeignBikePopup;
    let confirmedPopup;

    //Only show removeBike and needMaintenance-buttons if a bike is selected.
    if (this.selectedBikeID != -1) {
      bikeSpecificButtons = (
        <React.Fragment>
          <StdButton width="250px" height="40px" marginTop="20px" handleOnClick={this.togglePopupRemoveBike}>
            Remove bike
          </StdButton>
          <br />
          <StdButton width="250px" height="40px" handleOnClick={this.togglePopupMaintenance}>
            Needs Maintenance
          </StdButton>
        </React.Fragment>
      );
    } else bikeSpecificButtons = null;

    //Selective rendering of removeBikePopup
    if (this.showPopupRemoveBike) {
      removeBikePopup = (
        <RemoveBikePopup
          onRemoveBike={this.getBikes}
          onCancel={this.togglePopupRemoveBike}
          onConfirmed={this.onConfirmedRemoveBike}
        />
      );
    } else removeBikePopup = null;

    //Selective rendering of maintenancePopup
    if (this.showPopupMaintenance) {
      maintenanceBikePopup = (
        <NeedsMainenancePopup
          onNeedMaintenance={this.getBikes}
          onCancel={this.togglePopupMaintenance}
          onConfirmed={this.onConfirmedMaintenanceBike}
        />
      );
    } else maintenanceBikePopup = null;

    //Selective rendering of newBikePopup
    if (this.showPopupNewBike) {
      registerBikePopup = (
        <RegisterNewBikePopup
          onAddNewBike={this.getBikes}
          onCancel={this.togglePopupNewBike}
          onConfirmed={this.onConfirmedRegisterBike}
        />
      );
    } else registerBikePopup = null;

    //Selective rendering of foreignBikePopup
    if (this.showPopupForeignBike) {
      registerForeignBikePopup = (
        <RegisterNewForeignBikePopup
          onRegisterForeignBike={this.getBikes}
          onCancel={this.togglePopupForeignBike}
          onConfirmed={this.onConfirmedRegisterForeignBike}
        />
      );
    } else registerForeignBikePopup = null;

    //Confirmation popup that shows the user that the bike has been successfully registered in your current/loged in location.
    if (this.showPopupConfirmed) {
      confirmedPopup = (
        <PopupWindow height="180px" width="400px">
          <div style={{ color: 'white', textAlign: 'center' }}>
            <Container>
              <Row style={{ marginTop: '20px' }}>
                <Col>
                  <h3>Success</h3>
                </Col>
              </Row>
              <Row style={{ marginTop: '20px' }}>
                <Col sm={4} />
                <Col sm={4}>
                  <StdButton handleOnClick={this.togglePopupConfirmed} width="100px" height="50px">
                    Close
                  </StdButton>
                </Col>
                <Col sm={4} />
              </Row>
            </Container>
          </div>
        </PopupWindow>
      );
    } else confirmedPopup = null;
    //return the entire page
    return (
      <Container fluid={true}>
        <Row>
          <Col>
            <Search onSubmit={this.handleOnSerarch} />
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <div>
              {/*render the card area that contains all the bike cards */}
              <CardArea width="100%" height="80vh">
                {bikeCards}
              </CardArea>
            </div>
          </Col>
          <Col md={6}>
            <Row>
              <Col>
                <BikeInfoArea bikeInfo={this.bikeInfo}>
                  <ButtonGroup>{bikeSpecificButtons}</ButtonGroup>
                </BikeInfoArea>
              </Col>
            </Row>
            <Row>
              <Col>
                <ButtonGroup>
                  <StdButton width="250px" height="40px" marginTop="20px" handleOnClick={this.togglePopupNewBike}>
                    Register new bike
                  </StdButton>
                  <br />
                  <StdButton width="250px" height="40px" handleOnClick={this.togglePopupForeignBike}>
                    Register foreign bike
                  </StdButton>
                </ButtonGroup>
                {/* <BikeButtons onRemoveBike={this.getBikes} onAddNewBike={this.getBikes} onNeedMaintenance={this.getBikes} /> */}
              </Col>
            </Row>
          </Col>
        </Row>
        {/* the popups */}
        {removeBikePopup}
        {maintenanceBikePopup}
        {registerBikePopup}
        {registerForeignBikePopup}
        {confirmedPopup}
      </Container>
    );
  }
  //gets the biks from db when the page loads
  mounted() {
    this.getBikes();
  }
}
//the class for rendering the bike-info
class BikeInfoArea extends Component {
  //gets the info from the bike
  state = { info: this.props.bikeInfo };

  componentWillReceiveProps(nextProps) {
    this.setState({
      info: nextProps.bikeInfo
    });
  }

  render() {
    //css that makes the sites look the same, aswell as this area in particular has its own styling.
    const bikeInfoAreaStyle = {
      marginTop: '10px',
      textAlign: 'center',
      height: '50px',
      width: '100%',
      maxHeight: '50vh',
      color: 'white',
      padding: '0'
    };

    const divStyle = {
      borderStyle: 'solid',
      border: 'solid rgba(0,0,0,0.3) 2px',
      margin: 'auto',
      textAlign: 'center',
      padding: '0',
      marginTop: '15px',
      width: '100%',
      color: 'white'
    };

    return (
      <Container>
        <Row>
          <div style={divStyle}>
            <div style={bikeInfoAreaStyle}>
              <h3>Bike details</h3>
            </div>
            <p />
            {this.state.info}
            {this.props.children}
          </div>
        </Row>
      </Container>
    );
  }
}
//styling for buttons
class ButtonGroup extends Component {
  render() {
    let buttonGroupStyle = {
      textAlign: 'center'
    };
    return <div style={buttonGroupStyle}>{this.props.children}</div>;
  }
}
//class for remove bike popup
class RemoveBikePopup extends Component {
  scrappedBikeArray = [];

  //function when ok is clicked
  handleOnClickOk = () => {
    this.removeBike();
    this.props.onConfirmed();
  };

  //function when cancel is clicked
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  removeBike = () => {
    //the query, getting the bike-ID and marking it as scrapped
    dbServiceBikes.removeBike(thisBikeId, scrappedBikeArray => {
      this.scarppedBikeArray = scrappedBikeArray;
      this.props.onRemoveBike();
    });
  };

  render() {
    return (
      <div style={{ color: 'white', textAlign: 'center', margin: '0' }}>
        {/* The popup - remove bike */}

        <PopupWindow height="180px" width="580px">
          <h3 style={{ marginTop: '20px' }}>Are you sure you want to remove bike with id: {thisBikeId}</h3>

          <StdButton handleOnClick={this.handleOnClickOk} width="100px" height="50px">
            OK
          </StdButton>

          <StdButton handleOnClick={this.handleOnClickCancel} width="100px" height="50px">
            Cancel
          </StdButton>
        </PopupWindow>
      </div>
    );
  }
}
//class for sending a bike to maintenance
class NeedsMainenancePopup extends Component {
  //array made from query
  maintenanceBikes = [];
  //input variable
  maintenanceInput = '';

  //function when ok is clicked
  handleOnClickOk = () => {
    this.markMaintenance();
    this.props.onConfirmed();
  };

  //function when cancel is clicked
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  markMaintenance = () => {
    //the query for adding a bike to the maintenance-table in the DB
    dbServiceBikes.bikeNeedsMaintenance(this.maintenanceInput, thisBikeId, maintenanceBikes => {
      this.maintenanceBikes = maintenanceBikes;
    });

    //the query for changing the status of a bike to "need maintenance"
    dbServiceBikes.updateBikeStatusMaintenance(thisBikeId, maintenanceBikes => {
      this.maintenanceBikes = maintenanceBikes;
    });
    //calls the props to refresh the bike-list
    this.props.onNeedMaintenance();
  };

  render() {
    return (
      <div style={{ color: 'white', textAlign: 'center' }}>
        {/* The popup - bike needs maintenance */}

        <PopupWindow height="200px" width="600px">
          <p>Are you sure you want to send bike with bike id: {thisBikeId} to the workshop?</p>

          <Form.Group as={Col} controlId="text" onChange={event => (this.maintenanceInput = event.target.value)}>
            <Form.Control
              as="textarea"
              placeholder="What is wrong"
              aria-label="With textarea"
              style={{ maxHeight: '70px' }}
            />
          </Form.Group>

          <StdButton handleOnClick={this.handleOnClickOk} width="100px" height="50px">
            OK
          </StdButton>
          <StdButton handleOnClick={this.handleOnClickCancel} width="100px" height="50px" marginLeft="500px">
            Cancel
          </StdButton>
        </PopupWindow>
      </div>
    );
  }
}

//class for registering new bike
class RegisterNewBikePopup extends Component {
  //arrays from queries
  types = [];
  genders = [];
  //array made from query
  newBikeArray = [];
  //input variables
  chassienumber = '';
  make = '';
  model = '';
  name = '';
  age = [];
  size = '';
  level = [];
  bike_info = '';

  //form variables
  selectedGender = '';
  selectedType = '';
  selectedAge = '';
  selectedLevel = '';

  //function when ok is clicked
  handleOnClickOk = () => {
    this.addNewBike();
    this.props.onConfirmed();
  };

  //function when cancel is clicked
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  addNewBike = () => {
    //this should be a variable in Bike-Class
    this.setState({
      showPopupNewBike: !this.state.showPopupNewBike
    });
    //the query to add a new bike to the DB
    dbServiceBikes.addNewBike(
      this.chassienumber,
      this.name,
      this.selectedType,
      this.make,
      this.model,
      this.size,
      this.selectedLevel,
      this.selectedGender,
      this.selectedAge,
      this.bike_info,
      newBikeArray => {
        this.newBikeArray = newBikeArray;
        //calls the prop to refresh the bike-list
        this.props.onAddNewBike();
      }
    );
  };

  render() {
    console.log(this.selectedGender, 'gender');
    console.log(this.selectedType, 'type');
    console.log(this.selectedAge, 'age');
    return (
      //these forms are what comes up when you want to register a new bike. Every value from the database is here and needs to be filled in.
      <div style={{ color: 'white' }}>
        <PopupWindow height="550px" width="600px">
          <h4 style={{ marginTop: '10px', marginLeft: '10px' }}>New Bike</h4>
          <Form style={{ textAlign: 'center', marginLeft: '30px', marginRight: '30px', position: 'absolute' }}>
            <div style={{ marginLeft: '50px' }}>
              <Form.Row>
                <Form.Group as={Col} onChange={event => (this.selectedType = event.target.value)}>
                  <Form.Label>Type:</Form.Label>
                  <Form.Control as="select">
                    {/* sets the value to 1/2/3/4, but shows bmw, racer, offroad */}
                    {this.types.map(type => (
                      <option key={type.bike_typeID} value={type.bike_typeID}>
                        {type.bike_type}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>

                <Form.Group as={Col} onChange={event => (this.selectedGender = event.target.value)}>
                  <Form.Label>Gender:</Form.Label>
                  <Form.Control as="select">
                    {this.genders.map(gender => (
                      <option key={gender.gender_id} gender={gender.gender_name} value={gender.gender_id}>
                        {gender.gender_name}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>

                <Form.Group as={Col} onChange={event => (this.selectedAge = event.target.value)}>
                  <Form.Label>Age:</Form.Label>
                  <Form.Control as="select">
                    {this.age.map(age => (
                      <option key={age.age} value={age.user_age}>
                        {age.age}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.chassienumber = event.target.value)}>
                  <Form.Label>Chassienumber:</Form.Label>
                  <Form.Control type="text" placeholder="ex 1111" />
                </Form.Group>

                <Form.Group
                  as={Col}
                  controlId="text"
                  onChange={event => (this.make = event.target.value)}
                  style={{ marginLeft: '35px' }}
                >
                  <Form.Label>Make:</Form.Label>
                  <Form.Control type="text" placeholder="ex Xeedr" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="number" onChange={event => (this.model = event.target.value)}>
                  <Form.Label>Model:</Form.Label>
                  <Form.Control type="text" placeholder="ex 2" />
                </Form.Group>

                <Form.Group
                  as={Col}
                  controlId="number"
                  onChange={event => (this.size = event.target.value)}
                  style={{ marginLeft: '35px' }}
                >
                  <Form.Label>Size:</Form.Label>
                  <Form.Control type="text" placeholder="ex 20" />
                </Form.Group>
              </Form.Row>

              <Form.Row>
                <Form.Group as={Col} controlId="text" onChange={event => (this.bike_info = event.target.value)}>
                  <Form.Label>Bike details:</Form.Label>
                  <Form.Control as="textarea" aria-label="With textarea" style={{ maxHeight: '120px' }} />
                </Form.Group>
              </Form.Row>
            </div>
          </Form>

          <StdButton
            handleOnClick={this.handleOnClickCancel}
            width="100px"
            height="50px"
            float="right"
            marginRight="10px"
            marginTop="430px"
          >
            Cancel
          </StdButton>

          <StdButton
            width="100px"
            height="50px"
            handleOnClick={this.handleOnClickOk}
            float="right"
            marginRight="10px"
            marginTop="430px"
            postion="absolute"
          >
            OK
          </StdButton>
        </PopupWindow>
      </div>
    );
  }
  mounted() {
    //queries that collects the information we need from the database to be able to save a new bike, and that all the fill-out inputs gets a value
    dbServiceBikes.getBikeType(types => {
      this.types = types;

      this.types.map((item, i) => {
        if (i == 0) this.selectedType = item.bike_typeID;
      });
    });

    dbServiceBikes.getUserAge(age => {
      this.age = age;

      this.age.map((item, i) => {
        if (i == 0) this.selectedAge = item.user_age;
      });
    });

    dbServiceBikes.getUserLevel(level => {
      this.level = level;

      this.level.map((item, i) => {
        if (i == 0) this.selectedLevel = item.user_level;
      });
    });

    dbServiceBikes.getGender(genders => {
      this.genders = genders;

      this.genders.map((item, i) => {
        if (i == 0) this.selectedGender = item.gender_id;
      });
    });
  }
}

//class for registering a bike from a different location
class RegisterNewForeignBikePopup extends Component {
  //variable from input
  foreignBike = '';
  //gets the information about the bike selected
  foreignBikeArray = [];
  popupHeight = '200px';

  //function when ok is clicked
  handleOnClickOk = () => {
    this.props.onConfirmed();
    this.addForeignBike();
  };

  //function when cancel is clicked
  handleOnClickCancel = () => {
    this.props.onCancel();
  };

  //the query to pick out the chosen bike
  getForeignBike = () => {
    dbServiceBikes.getForeignBike(this.foreignBike, foreignBikeArray => {
      this.foreignBikeArray = foreignBikeArray;
    });
    this.popupHeight = '420px';
  };

  addForeignBike = () => {
    //the query to change the adress to a bike to the login-address
    dbServiceBikes.addForeignBike(this.foreignBike, success => {
      this.props.onRegisterForeignBike();
    });
  };

  render() {
    foreignFilter = this.foreignBikeArray;
    foreignBikeComponent = foreignFilter.map(item => (
      <StdCard selectable={true} bikeId={item.bike_id} key={item.bike_id} transport={item}>
        <h1>{item.make}</h1>
        <a>ID:{item.bike_id}</a>
        <br />
        <a>Location: {item.zip_location}</a>
        <br />
        <a>Chassienumber: {item.chassienum}</a>
        <br />
        <a>Status: {item.status}</a>
      </StdCard>
    ));
    return (
      //area where your register the bike that has been handled in by the costumer.
      <div style={{ color: 'white', textAlign: 'center' }}>
        <PopupWindow height={this.popupHeight} width="600px">
          <p>Chassienumber:</p>
          <input onChange={event => (this.foreignBike = event.target.value)} />
          <StdButton handleOnClick={this.getForeignBike} width="100px" height="50px">
            Search
          </StdButton>
          <br />
          <div style={{ color: 'black' }}>{foreignBikeComponent}</div>
          <StdButton
            handleOnClick={this.handleOnClickCancel}
            width="100px"
            height="50px"
            marginLeft="500px"
            float="right"
            marginRight="10px"
            marginTop="10px"
          >
            Cancel
          </StdButton>
          <StdButton
            handleOnClick={this.handleOnClickOk}
            width="100px"
            height="50px"
            marginLeft="450px"
            float="right"
            marginRight="10px"
            marginTop="10px"
          >
            OK
          </StdButton>
        </PopupWindow>
      </div>
    );
  }
}
export default Bikes;
