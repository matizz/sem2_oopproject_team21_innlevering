import React from 'react';
import { Component } from 'react-simplified';
import MaintenanceBikes from './maintenance_bikes';

class MaintenanceIndex extends Component {
  state = {};
  render() {
    return (
      <div>
        <div>
          <MaintenanceBikes />
        </div>
      </div>
    );
  }
}

export default MaintenanceIndex;
