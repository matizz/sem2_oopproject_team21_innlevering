import React from 'react';
import { Component } from 'react-simplified';
import date from "date-and-time";
//imports db-queries
import { dbServiceMaintenance } from '../../Services_maintenance/service_maintenanceQueries';
import { maintenanceSort } from './../../Services_maintenance/service_maintenanceSort';
//imports standard-components
import StdCard from '../../StandardComponents/stdCard';
import CardArea from '../../StandardComponents/stdCardArea';
import StdButton from '../../StandardComponents/stdButton';
//imports bootstrap
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
//imports package to show a loading-icon while waiting for database
import ReactLoading from 'react-loading';

class MaintenanceBikes extends Component {
  //info about the selected bike
  currentBikeInfo = '';
  //control if button is visible or not
  selectedBikeId = -1;
  oldVsNewRepairs = 0;
  allRepairs = [];
  currentSortMode = 'Date (newest)';

  //arrays with data from db
  filteredRepairs = [];
  deletedBikes = [];
  updatedBikes = [];
  gettingRepairs = false;

  //handler for sorting array with reps
  handleSort(sort) {
    if (sort === 'Date (oldest)')
      //sort the repairs by what the user selects
      this.filteredRepairs = maintenanceSort.sortByOldest(this.allRepairs);
    else this.filteredRepairs = maintenanceSort.sortByNewest(this.allRepairs);
  }

  //maked the buttons on this site work and collect the information we want to be seen.
  getRepairs = () => {
    this.gettingRepairs = true;
    this.oldVsNewRepairs = 0;
    this.allRepairs = [];

    dbServiceMaintenance.getMaintenanceBikes(bikes => {
      this.allRepairs = bikes;
      this.gettingRepairs = false;
      this.handleSort(this.currentSortMode);
    });
  };

  //gets finished reparis
  getOldRepairs = () => {
    this.gettingRepairs = true;
    this.oldVsNewRepairs = 1;
    this.currentBikeInfo = '';
    this.allRepairs = [];

    dbServiceMaintenance.getOldRepairs(bikes => {
      this.gettingRepairs = false;
      this.allRepairs = bikes;
      this.handleSort(this.currentSortMode);
    });
  };

  //stores bike-id for later use
  getSelectedBikeInfo = (info, id) => {
    this.selectedBikeId = id;
    this.currentBikeInfo = info;
  };

  //changes the status of the bike from 3 to normal status
  fixed = () => {
    dbServiceMaintenance.maintenanceDone(this.selectedBikeId, deletedBikes => {
      this.deletedBikes = deletedBikes;
    });
    dbServiceMaintenance.updatebikeFixed(this.selectedBikeId, updatedBikes => {
      this.updatedBikes = updatedBikes;
    });
    this.mounted();
  };
  //if the bike is no longer usable, this is where we discard it after having a look at it in the workshop.
  scrapped = () => {
    dbServiceMaintenance.maintenanceDone(this.selectedBikeId, deletedBikes => {
      this.deletedBikes = deletedBikes;
    });

    dbServiceMaintenance.updateBikeScrapped(this.selectedBikeId, updatedBikes => {
      this.updatedBikes = updatedBikes;
    });
    this.mounted();
  };

  //to show current repairs
  handleOnClickCurrentRepairs = () => {
    this.selectedBikeId = -1;
    this.currentBikeInfo = '';
    this.getRepairs();
  };

  render() {
    let repCards;
    let bikeSpecificButtons;

    const selector = {
      margin: '0',
      marginLeft: '30px',
      width: '25%'
    };

    const infoDiv = {
      borderStyle: 'solid',
      border: 'solid rgba(0,0,0,0.3) 2px',
      margin: 'auto',
      textAlign: 'center',
      padding: '0',
      marginTop: '15px',
      width: '100%',
      color: 'white',
      marginLeft: '15px',
      marginRight: '15px'
    };

    const bikeInfoAreaStyle = {
      marginTop: '10px',
      textAlign: 'center',
      height: '50px',
      width: '100%',
      maxHeight: '50vh',
      padding: '0'
    };

    if (this.filteredRepairs.length > 0)
      repCards = this.filteredRepairs.map(ticket => (
        <BikeCard
          handleOnClick={this.getSelectedBikeInfo}
          bikeInfo={ticket.rep_description}
          sykkelid={ticket.bike_id}
          key={ticket.rep_id}
          item={ticket}
        />
      ));
    else if (this.gettingRepairs)
      //show a spinner while the query runs
      repCards = (
        <div width="100%" style={{ width: '13.5%', margin: '20vh auto' }}>
          <ReactLoading type="spin" color="black" delay={250} />
        </div>
      );
    //no bikes matching search
    else
      repCards = (
        <div style={{ textAlign: 'center', color: 'white', margin: '20vh auto' }}>No bikes mathcing your search</div>
      );

    //the choises you have after a bike has been sent to maintenance
    if (this.selectedBikeId != -1 && this.oldVsNewRepairs == 0) {
      bikeSpecificButtons = (
        <div style={{ textAlign: 'center' }}>
          <StdButton width="250px" height="40px" marginTop="10px" handleOnClick={this.fixed}>
            Register as fixed
          </StdButton>
          <br />
          <StdButton width="250px" height="40px" handleOnClick={this.scrapped}>
            Scrap
          </StdButton>
        </div>
      );
    } else bikeSpecificButtons = null;

    return (
      //shows a status on for how long the bike has had status 3(maintenance) using the registered date, and you might compare it to todays date.
      <Container fluid={true}>
        <Form>
          <Form.Group controlId="formHorizontalEmail" style={{ margin: '0' }}>
            <Form.Control
              as="select"
              style={selector}
              onChange={event => {
                this.handleSort(event.target.value);
                this.currentSortMode = event.target.value;
              }}
            >
              <option>Date (newest)</option>
              <option>Date (oldest)</option>
            </Form.Control>
          </Form.Group>
        </Form>

        <Row>
          <Col md={6}>
            <CardArea width="100%" height="80vh">
              {repCards}
            </CardArea>
          </Col>

          <Col md={6}>
            <Row>
              <div style={infoDiv}>
                <div>
                  <h3 style={bikeInfoAreaStyle}>Maintenance details</h3>
                </div>
                <p style={{ textAlign: 'center' }} />
                {this.currentBikeInfo}
                {bikeSpecificButtons}
              </div>
            </Row>

            <Row>
              <Col style={{ textAlign: 'center', marginTop: '10px' }}>
                <StdButton width="250px" height="40px" handleOnClick={this.getOldRepairs}>
                  Old repairs
                </StdButton>
              </Col>
            </Row>
            <Row>
              <Col style={{ textAlign: 'center' }}>
                <StdButton width="250px" height="40px" handleOnClick={this.handleOnClickCurrentRepairs}>
                  Current repairs
                </StdButton>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    );
  }
  mounted() {
    this.getRepairs();
  }
}

class BikeCard extends Component {
  //sends bikeid
  handleOnClick = () => {
    this.props.handleOnClick(this.props.bikeInfo, this.props.sykkelid);
  };

  render() {
    // formatting date
    let toDate;
    if (new Date(this.props.item.to_date).toDateString() == 'Thu Jan 01 1970') {
      toDate = ' ';
    } else {
      toDate = date.format(new Date(this.props.item.to_date), 'DD/MM-YYYY');
    }

    return (
      //what shows on the cards in the onload site
      <StdCard onClick={this.handleOnClick} selectable={true} margin="15px">
        <Card.Header>
          <h1>Ticket: {this.props.item.rep_id}</h1>
        </Card.Header>
        <Card.Body>
          <Container fluid={true}>
            <Row>
              <Col sm={4}>
                <a>Bike:</a>
                <br />
                <a>Bike ID:</a>
                <br />
                <a>Chassie nr:</a>
                <br/>
                <a>From:</a>
                <br />
                <a>To:</a>
              </Col>
              <Col sm={8}>
                {this.props.item.make} {this.props.item.name}
                <br />
                {this.props.item.bike_id}
                <br/>
                {this.props.item.chassienum}
                <br />
                {date.format(new Date(this.props.item.from_date), 'DD/MM-YYYY')}
                <br />
                {toDate}
              </Col>
            </Row>
          </Container>
        </Card.Body>
      </StdCard>
    );
  }
}

export default MaintenanceBikes;
