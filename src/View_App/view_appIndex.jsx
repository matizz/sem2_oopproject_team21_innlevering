import * as React from 'react';
import { Component } from 'react-simplified';
import { Route } from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';
import ordersIndex from './Tabview_Orders/tabview_ordersIndex';
import Transports from './Tabview_Transport/transports';
import { loginService } from './../Services_Login/Service_login';
import BikeIndex from './Tabview_Bikes/tabview_bikeIndex';
import MaintenanceIndex from './Tabview_Maintenance/tabview_maintenanceIndex';
import StatusIndex from './Tabview_Status/tabview_statusIndex';
import Customers from './Tabview_Customers/tabview_customers';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
//import a package to format date
import Clock from 'react-live-clock';

const history = createHashHistory();

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <NavBar />
        <div>
          <Route exact path={this.props.match.path} component={StatusIndex} />
          <Route path={`${this.props.match.path}/Bikes`} component={BikeIndex} />
          <Route path={`${this.props.match.path}/Orders`} component={ordersIndex} />
          <Route path={`${this.props.match.path}/Transport`} component={Transports} />
          <Route path={`${this.props.match.path}/Maintenance`} component={MaintenanceIndex} />
          <Route path={`${this.props.match.path}/Customers`} component={Customers} />
        </div>
      </div>
    );
  }
}

const navStyle = {
  //makes changes to the bar itself.
  listStyleType: 'none',
  margin: '0',
  marginBottom: '20px',
  width: '100%',
  height: '50px'
};

const navItemStyle = {
  //makes changes to every single unique item in the navbar, ex: Bikes.
  display: 'block',
  padding: '8px 16px',
  textAlign: 'center',
  display: 'inline',
  float: 'left',
  marginTop: '10px',
  marginLeft: '20px',
  color: 'white'
};

const loggedInStyle = {
  //styles the "logged in as:" item.
  display: 'block',
  padding: '8px 16px',
  textDecoration: 'none',
  textAlign: 'center',
  display: 'inline',
  float: 'right',
  marginTop: '10px',
  color: 'white'
};

const dateStyle = {
  //styles the "logged in as:" item.
  display: 'block',
  padding: '8px 16px',
  textDecoration: 'none',
  textAlign: 'center',
  display: 'inline',
  float: 'right',
  marginTop: '10px',
  color: 'white',
  marginRight: '50px',
  marginLeft: '50px'
};

class NavBar extends Component {
  handleOnclick = () => {
    loginService.HandleLogout(() => {
      history.push('/Login');
    });
  };

  render() {
    return (
      <div style={navStyle}>
        <Navbar>
          <Nav fill variant="tags" className="mr-auto" variant="tabs" >
            <Nav.Item>
              <Nav.Link  style={navItemStyle} onClick={() => history.push('/App') }>
                Status
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={() => history.push('/App/Bikes')}>
                Bikes
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={() => history.push('/App/Orders')}>
                Orders
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={() => history.push('/App/Transport')}>
                Transport
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={() => history.push('/App/Maintenance')}>
                Maintenance
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={() => history.push('/App/Customers')}>
                Customers
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link style={navItemStyle} onClick={this.handleOnclick} >
                Log out
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <Clock style={dateStyle} format={'DD/MM-YYYY HH:mm:ss'} ticking={true} />
          <br />
          <a style={loggedInStyle}>Logged inn as: {localStorage.getItem('loginUser')}</a>
        </Navbar>
      </div>
    );
  }
}

export default App;
