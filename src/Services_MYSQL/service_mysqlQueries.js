import {
  connection
} from './service_mysqlConnection';

class DatabaseService {
  GetPassword(user, callBack) {
    connection.query('SELECT `password` FROM `users` WHERE username=?', [user], (error, results) => {
      if (error) return console.error(error);

      if (results.length > 0) callBack(results[0].password);
      else callBack();
    });
  }

  getBikes(success) {
    connection.query('select * from bikes, locations, bike_type, user_age, status, user_gender  where locations.adress = bikes.adress and bikes.type = bike_type.bike_typeID and bikes.user_age = user_age.user_age and bikes.status = status.status_id and bikes.user_gender = user_gender.gender_id and status not like 4 ', (error, results) => {
      if (error) return console.error(error);

      success(results);
    });
  }

  selectBikes(id, success) {
    connection.query(
      'SELECT * from bikes, locations WHERE locations.adress = bikes.adress AND bikes.bike_id=?',
      [id],
      (error, results) => {
        if (error) return console.error(error);

        success(results);
      }
    );
  }

  // getMaintenanceInfo(id, success) {
  //   connection.query('select * from reparis where id=?', [id], (error, results) => {
  //     if (error) return console.error(error);

  //     success(results);
  //   });
  // }
}
export let dbService = new DatabaseService();