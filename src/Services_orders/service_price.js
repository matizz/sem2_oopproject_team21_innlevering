
import { connection } from './../Services_MYSQL/service_mysqlConnection';


class PriceService {

    price = [];
    ageModifier = [];
    levelModifier = [];

    //get the price values from the database
    init() {

        //get base prices
        connection.query('SELECT * FROM bike_type', (error, results) => {
            if (error) return console.error(error);

            let id = 0;
            let price = 0;
            this.basePrice = [];

            for (let i = 0; i < results.length; i++) {
                const element = results[i];

                let basePriceObj = new Object();
                basePriceObj.baseId = element.bike_typeID;
                basePriceObj.price = element.base_price;

                this.price.push(basePriceObj);
            }
        });

        //get age modifiers
        connection.query('SELECT * FROM user_age', (error, results) => {
            if (error) return console.error(error);

            for (let i = 0; i < results.length; i++) {
                const element = results[i];

                let ageModObj = new Object();
                ageModObj.ageId = element.user_age;
                ageModObj.priceMod = element.price_modifier;

                this.ageModifier.push(ageModObj);
            };
        });

        //get level modifiers
        connection.query('SELECT * FROM user_level', (error, results) => {
            if (error) return console.error(error);

            let id = 0;
            let priceMod = 0;
            let levelModifierObject = {};

            for (let i = 0; i < results.length; i++) {
                const element = results[i];

                let levelModObj = new Object();
                levelModObj.lvlId = element.user_level;
                levelModObj.priceMod = element.price_modifier;

                this.levelModifier.push(levelModObj);
            };
        });
    }

    calculateBikePrice(bike) {
        let bikeType = bike.type;
        let bikeLevel = bike.user_level;
        let bikeAge = bike.user_age;

        let finalPrice = 0;
        //calc baseprice
        for (let key in this.price) {
            if (this.price[key].baseId = bikeType)
                finalPrice = this.price[key].price;

        }

        //add age modifier
        for (let key in this.ageModifier) {
            if (this.ageModifier[key].ageId == bikeAge) {

                finalPrice *= this.ageModifier[key].priceMod;
            }
        }

        //add level modifier
        for (let key in this.levelModifier) {

            if (this.levelModifier[key].lvlId == bikeLevel) {

                finalPrice *= this.levelModifier[key].priceMod;

            }
        }
        console.log("price", finalPrice)
        return finalPrice;
    }
}

export let priceService = new PriceService();