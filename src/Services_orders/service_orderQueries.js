import {
    connection
} from '../Services_MYSQL/service_mysqlConnection'

class OrderService {
    getAvailableBikes(location, from, to, success) {
        connection.query('SELECT DISTINCT b.* FROM orders o INNER JOIN bike_order bo ON o.order_id=bo.order_id RIGHT JOIN bikes b ON bo.bike_id=b.bike_id WHERE b.current_adress =? AND b.adress =? AND b.status != 3 AND b.status != 4 AND bo.bike_id NOT IN (SELECT DISTINCT bo.bike_id FROM orders o, bike_order bo WHERE o.order_id = bo.order_id AND ((? >= o.from_date AND o.to_date >= ?) OR (o.from_date <= ? AND o.to_date >= ?) OR (o.from_date <= ? AND o.to_date >= ?) OR (o.from_date >= ? AND o.to_date <= ?)))', [location, location, from, from, from, to, to, to, from, to], (error, results) => {
            if (error) return console.error(error);

            success(results);
        });
    }

    getLocation(success) {
        connection.query('SELECT adress FROM users WHERE username = ?', [localStorage.getItem('loginUser')], (error, results) => {
            if (error) return console.error(error);

            success(results)
        });
    }

    getCustomers(success) {
        connection.query('SELECT * FROM customers', (error, results) => {
            if (error) return console.error(error);

            success(results);
        });
    }

    getCustomerID(success) {
        connection.query('SELECT customer_id FROM customers WHERE customer_id=(SELECT max(customer_id) FROM customers)',
            (error, results) => {
                if (error) return console.error(error);

                success(results);
            }
        )
    }

    selectCustomer(id, success) {
        connection.query('SELECT * FROM customers WHERE customer_id=?', [id], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    orderNewC(fName, lName, dob, phone, email, id, success) {
        connection.query('INSERT INTO customers (name, email, cell_num, date_ob, person_id) VALUES (?, ?, ?, ?, ?)',
            [fName + ' ' + lName, email, phone, dob, id],
            (error) => {
                if (error) return console.error(error);

                success();
            }
        )
    }

    newOrder(from, to, comment, customer_id, fulfilled, success) {
        connection.query('INSERT INTO orders (from_date, to_date, order_comment, customer_id, fulfilled) VALUES (?, ?, ?, ?, ?)',
            [from, to, comment, customer_id, fulfilled],
            (error) => {
                if (error) return console.error(error);

                success();
            }
        )
    }

    getOrderID(success) {
        connection.query('SELECT order_id FROM orders WHERE order_id=(SELECT max(order_id) FROM orders)',
            (error, results) => {
                if (error) return console.error(error);

                success(results);
            }
        )
    }

    getOrder(id, success) {
        connection.query('SELECT * FROM orders WHERE order_id=?', [id],
            (error, results) => {
                if (error) return console.error(error);

                success(results);
            }
        )
    }

    orderBike(bike_id, order_id) {
        connection.query('INSERT INTO bike_order (bike_id, order_id) VALUES (?, ?)', [bike_id, order_id],
            (error) => {
                if (error) return console.error(error);
            }
        )
    }

    orderEquipment(eq_id, order_id, amount) {
        connection.query('INSERT INTO equipment_order (eq_id, order_id, amount) VALUES (?, ?, ?)', [eq_id, order_id, amount],
            (error) => {
                if (error) return console.error(error);
            }
        )
    }

    getEquipment(success) {
        connection.query('SELECT * from equipment', (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    selectEquipment(id, success) {
        connection.query('SELECT * FROM equipment WHERE eq_id=?', [id], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    selectOrderEquipment(id, success) {
        connection.query('SELECT * FROM equipment_order WHERE order_id=?', [id], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    getOrders(success) {
        connection.query('select * from orders, order_fulfilled where orders.fulfilled = order_fulfilled.fulfilled_id and fulfilled !=3', (error, results) => {
            if (error) return console.log(error);

            success(results);
        });
    }

    getBikeOrder(id, success) {
        connection.query('SELECT bike_id FROM bike_order WHERE order_id=?', [id], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }

    updateOrder(id, comment, fromDate, toDate) {
        connection.query('UPDATE orders SET from_date=?, to_date=?, order_comment=? WHERE order_id=?', [fromDate, toDate, comment, id], (error, results) => {
            if (error) return console.error(error);
        })
    }

    deleteBikeOrder(id) {
        connection.query('DELETE FROM bike_order WHERE order_id=?', [id], (error) => {
            if (error) return console.log(error);
        });
    }

    deleteEquipmentOrder(id) {
        connection.query('DELETE FROM equipment_order WHERE order_id=?', [id], (error) => {
            if (error) return console.log(error);
        });
    }

    deleteOrder(id) {
        connection.query('UPDATE orders SET fulfilled=3 WHERE order_id=?', [id], (error) => {
            if (error) return console.error(error);
        })
    }

    deliverOrder(fulfilled, id) {
        connection.query('UPDATE orders SET fulfilled=? WHERE order_id=?', [fulfilled, id], (error) => {
            if (error) return console.log(error);
        })
    }

    deliverBike(status, id) {
        connection.query('UPDATE bikes b SET status = ? WHERE EXISTS (SELECT b.* FROM orders o, bike_order bo WHERE bo.order_id = o.order_id AND bo.bike_id = b.bike_id AND o.order_id = ?)', [status, id], (error) => {
            if (error) return console.log(error);
        })
    }
}

export let orderService = new OrderService;