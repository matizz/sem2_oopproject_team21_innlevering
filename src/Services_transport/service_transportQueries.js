import {
    connection
} from '../Services_MYSQL/service_mysqlConnection';

class DatabaseService {

    getBikesCity(success) {
        //get the current adress of the logedin user
        connection.query(
            'select adress from users where username = ?',
            [localStorage.getItem('loginUser')],
            (error, results) => {
                if (error) return console.error(error);
                let address = results[0].adress;

                //get all the bikes that does not belong on the current address
                connection.query(
                    'select bike_id, make, zip_location, bikes.current_adress, bikes.adress from bikes, locations where locations.adress = bikes.adress and locations.zip_location!=? and bikes.current_adress=?',
                    [address, address],

                    (error, results) => {
                        if (error) return console.error(error);

                        success(results);
                    }
                );
            }
        );
    }

    getCityInfo(city, success) {
        connection.query('select info from locations where zip_location=?', [city], (error, results) => {
            if (error) return console.error(error);

            success(results);
        });
    }

    getCities(success) {
        connection.query('select zip_location from locations', (error, results) => {
            if (error) return console.error(error);

            success(results);
        });
    }

    simulateTransport(currentAddress, address, success) {
        connection.query('update bikes set current_adress=? where adress=?', [currentAddress, address], (error, results) => {
            if (error) return console.error(error);

            success(results);
        })
    }




}
export let dbServiceTransport = new DatabaseService();