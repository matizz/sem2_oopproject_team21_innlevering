

class DateService {

    sqlDateToDays(date) {

        let now = date;
        let start = new Date(now.getFullYear(), 0, 0);
        let diff = now - start;
        let oneDay = 1000 * 60 * 60 * 24;
        let day = Math.floor(diff / oneDay);
        return day;
    }

    sqlDateToYear(date) {
        let now = date;
        let start = new Date(now.getFullYear(), 0, 0);
        let diff = now - start;
        let oneDay = 1000 * 60 * 60 * 24;
        let day = Math.floor(diff / oneDay);
        let year = new Date().getFullYear();

        //add the year to the date to create a sortable integer
        return (year + day);
    }
    getDayDifference(day1, day2) {
        return day1 - day2;
    }
}

export let dateService = new DateService();