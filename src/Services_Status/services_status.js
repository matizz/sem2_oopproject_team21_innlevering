import { connection } from '../Services_MYSQL/service_mysqlConnection';
import { dateService } from './services_date';
import { date } from 'date-and-time';

class StatusService {
    allNotifications = [];

    orderNotifications = [];
    bikeNotifications = [];
    repNotifications = [];

    queryNotifications(callback) {
        this.allNotifications = [];

        //using promises to avoid pyramid of doom of callbacks
        this.scanForOrderConflicts().then((result) => {
            this.allNotifications.push(result);
            return this.scanForOldRepTickets(result);
        }).then(result => {
            this.allNotifications.push(result);
            return this.scanForLateBikeReturns(result);
        }).then(result => {
            this.allNotifications.push(result);
            return this.scanForOrdersNotPickedUp(result);
        }).then(finalResult => {
            this.allNotifications.push(finalResult);
            callback(this.allNotifications);
        })
    }

    //scan the entire system for conflicts that users must be notified of
    scanForOrderConflicts() {
        return new Promise((resolve, reject) => {
            //query order conflicts
            connection.query('SELECT b.bike_id, b.status, o.order_id, o.fulfilled, o.from_date, o.to_date, DATEDIFF(o.from_date, CURRENT_DATE) as Delta FROM bikes b INNER JOIN bike_order bo ON b.bike_id = bo.bike_id INNER JOIN orders o ON o.order_id = bo.order_id where DATEDIFF(o.from_date, CURRENT_DATE) BETWEEN 0 AND 15 AND b.status != 1 AND o.fulfilled = 0 ', (error, results) => {
                if (error) {
                    reject();
                    return console.error(error);
                }

                let resultArr = []
                //parse every query result and add them to the notification array
                parseBikeUnavalible(results).map(result => {

                    resultArr.push(result);
                });
                resolve(resultArr);
            });
        })
    };

    scanForOldRepTickets(p) {
        return new Promise((resolve, reject) => {
            //query unfinished repairs
            connection.query('SELECT * FROM `repairs` WHERE to_date IS NULL', (error, results) => {
                if (error) {
                    reject();
                    return console.error(error);
                }

                let resultArr = [];
                parseRepairOutOfDate(results).map(result => {
                    resultArr.push(result);
                });

                resolve(resultArr);
            });
        });

    }

    scanForLateBikeReturns(p) {
        return new Promise((resolve, reject) => {
            //query unfinished repairs
            connection.query("SELECT b.bike_id, b.status, o.order_id, o.fulfilled, o.from_date, o.to_date, DATEDIFF(o.from_date, CURRENT_DATE) as Delta, c.cell_num, c.name FROM bikes b INNER JOIN bike_order bo ON b.bike_id = bo.bike_id INNER JOIN orders o ON o.order_id = bo.order_id INNER JOIN customers c ON o.customer_id = c.customer_id  where DATEDIFF(o.to_date, CURRENT_DATE) < 0 AND o.fulfilled = 1", (error, results) => {
                if (error) {
                    reject();
                    return console.error(error);
                }

                let resultArr = [];
                parseBikeNotReturned(results).map(result => {
                    resultArr.push(result);
                });

                resolve(resultArr);
            });
        });
    }

    scanForOrdersNotPickedUp(p) {
        return new Promise((resolve, reject) => {
            //query unfinished repairs
            connection.query("SELECT o.order_id, o.fulfilled, o.from_date, o.to_date, DATEDIFF(o.from_date, CURRENT_DATE) as Delta, c.name, c.cell_num FROM orders o  INNER JOIN customers c  ON c.customer_id = o.customer_id where DATEDIFF(o.from_date, CURRENT_DATE) < 0 AND o.fulfilled = 0", (error, results) => {
                if (error) {
                    reject();
                    return console.error(error);
                }

                let resultArr = [];
                parseOrderNotPickedUp(results).map(result => {
                    resultArr.push(result);
                });

                resolve(resultArr);
            });
        });
    }
}

//====Parsers====\\

function parseBikeUnavalible(results) {
    let parsedQueries = [];

    for (let i = 0; i < results.length; i++) {
        const element = results[i];

        let notification = {
            title: "Bike unavalible",
            description: `There is an conflict on order: ${element.order_id}. Bike: ${element.bike_id} is not avalible for this order, please do something about that`,
            type: "orderConflict",
        }
        parsedQueries.push(notification);
    }

    return parsedQueries;
}

function parseBikeNotReturned(results) {
    let parsedQueries = [];

    for (let i = 0; i < results.length; i++) {
        const element = results[i];

        let notification = {
            title: "Bike not returned",
            description: `Bike: ${element.bike_id} on order ${element.order_id} is overdue to be returned. Please contact ${element.name} on ${element.cell_num}`,
            type: "oldRepTicket",
        }
        parsedQueries.push(notification);

    }

    return parsedQueries;
}

function parseOrderNotPickedUp(results) {
    let parsedQueries = [];

    for (let i = 0; i < results.length; i++) {
        const element = results[i];

        let notification = {
            title: "Order not picked up",
            description: `Order ${element.order_id} has not been picked up on time. Please contact ${element.name} on ${element.cell_num}`,
            type: "oldRepTicket",
        }
        parsedQueries.push(notification);

    }

    return parsedQueries;
}


function parseRepairOutOfDate(results) {
    let parsedQueries = [];

    for (let i = 0; i < results.length; i++) {
        const element = results[i];
        let notification;
        let sqlDate = element.from_date;

        let daydiff = dateService.getDayDifference(dateService.sqlDateToDays(new Date()), dateService.sqlDateToDays(sqlDate));

        //if there is more than a week since creation of the rep ticket
        if (daydiff >= 7) {
            notification = {
                title: "Repair out of date",
                description: `Rep ticket ${element.rep_id} is: ${daydiff} days old. This should be adressed shortly`,
                type: "oldRepTicket",
            }
            parsedQueries.push(notification);
        }
    }

    return parsedQueries;
}
function orderNotPickedUp() {
    let parsedQueries = [];

    for (let i = 0; i < results.length; i++) {
        const element = results[i];
        let notification;
        let sqlDate = element.from_date;

        let daydiff = dateService.getDayDifference(dateService.sqlDateToDays(new Date()), dateService.sqlDateToDays(sqlDate));

        //if there is more than a week since creation of the rep ticket
        if (daydiff >= 7) {
            notification = {
                title: "Rep ticket",
                description: `Rep ticket ${element.rep_id} is: ${daydiff} days old. This should be adressed shortly`,
                type: "oldRepTicket",
            }
            parsedQueries.push(notification);
        }
    }

    return parsedQueries;
}

export let statusService = new StatusService();
