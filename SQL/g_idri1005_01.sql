-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 10. Apr, 2019 15:48 PM
-- Tjener-versjon: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `g_idri1005_01`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bikes`
--

CREATE TABLE `bikes` (
  `bike_id` int(11) NOT NULL,
  `chassienum` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` int(11) NOT NULL,
  `make` varchar(50) NOT NULL,
  `model` varchar(50) NOT NULL,
  `size` int(11) NOT NULL,
  `user_level` int(11) NOT NULL,
  `user_gender` int(11) NOT NULL,
  `user_age` int(11) NOT NULL,
  `adress` varchar(50) NOT NULL,
  `current_adress` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `bike_info` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `bikes`
--

INSERT INTO `bikes` (`bike_id`, `chassienum`, `name`, `type`, `make`, `model`, `size`, `user_level`, `user_gender`, `user_age`, `adress`, `current_adress`, `status`, `bike_info`) VALUES
(20, '1111', 'CORE', 2, 'DBS', '3.0', 11, 2, 2, 1, 'Museumvegen', 'Museumvegen', 3, 'This bike works almost exclusively off-road. The wheels are really thick and heavy. The size of the bike is regular, fits women from 165-175. Adjustable seat and handle bar. Does not allow purses.'),
(21, '3444', 'SST', 1, 'Fujii', '2.0', 32, 0, 1, 0, 'Klostergaten 56', 'Trondheim', 1, 'This bike fits good for down-hill and parks. Its heavy and rough, fits men between 180-210cm.'),
(22, '3442', 'CRB', 2, 'Xeedr', '1.0', 33, 2, 2, 1, 'Karl Johansgate 1', 'Karl Johansgate 1', 3, 'Offroad-bike for women, fits 160-175cm. Rough and heavy, though lighter than the mens bike. Not adjustable seat and handle bar. Does not go with purses.'),
(23, '6666', 'Rubaix', 3, 'Fuji', '2.0', 13, 2, 2, 1, 'Nordre Fjellbergodden', 'Nordre Fjellbergodden', 1, 'Light and fast road-bike for women. Fits 170-180cm. Adjustable seat and handles, you may add purses.'),
(24, '8989', 'Scale', 2, 'Scott', '1.0', 10, 0, 1, 2, 'Museumvegen', 'Trondheim', 1, 'Off-road bike for men. Fits 165-175, heavy bike. Not adjustable seat and handles. Not possible with purses.'),
(25, '5556', 'Bolt', 1, 'Xeed', '1.0', 16, 2, 2, 2, 'Trondheim', 'Trondheim', 1, 'Fits women, 175-185cm. Havy bike, not adjustable seat and handle bar. Not possible with purses.'),
(26, '4532', 'Spark', 2, 'Scott', '1.4', 24, 1, 3, 2, 'Trondheim', 'Trondheim', 1, 'This is one of the Uni-sex bikes. Fits 160-180cm tall persons. Lighter than the male-offroad bike, but heavyer than the females. Does not go with purses.'),
(27, '2282', 'Eagle', 3, 'Xeed', '5.0', 22, 0, 3, 2, 'Trondheim', 'Trondheim', 1, 'Uni-sex bike, fast and light. Fits people from 170-190cm. Adjustable seat and handle-bar. Does not go with purses.'),
(28, '9999', 'Phoenix', 2, 'Merida', '2.0', 24, 1, 2, 1, 'Trondheim', 'Trondheim', 1, 'This bike will make it through'),
(29, '1337', 'Intruder', 3, 'DBS', '1.0', 32, 1, 2, 1, 'Trondheim', 'Trondheim', 3, 'Light bike for youths, fits youts between 150cm-170cm. You might add purses.'),
(30, '4206', 'CORE BAD', 2, 'DBS', '2.0', 29, 1, 2, 2, 'Trondheim', 'Trondheim', 1, 'Strong off-road bike, fits females between 165-175cm. Does not allow purses.'),
(31, '1234', 'XC', 2, 'Xeed', '1.0', 23, 1, 2, 1, 'Trondheim', 'Trondheim', 1, 'This bike fits girls age up to 17y, with height between 150-165cm. Does not allow purses.'),
(32, '1780', 'Contessa', 2, 'Scott', '2', 34, 1, 2, 1, 'Trondheim', 'Trondheim', 1, 'Womens bike, heavy and steady. Fits people between 160-170cm. Does not allow purses.'),
(33, '5643', 'Scultura', 3, 'Merida', '1.0', 28, 1, 1, 2, 'Trondheim', 'Trondheim', 1, 'Fast racer bike which fits men between 170-190cm. Extra light which makes it easier to earn a higher speed. Does not fit with purses.'),
(34, '8999', 'Sierra', 1, 'Merida', 'Gold', 33, 0, 1, 0, 'Trondheim', 'Trondheim', 4, 'This is a desert bmx. Perfect in the sand dunes, at the beach and even in the pub!'),
(35, '5656', 'Racer', 1, 'Scott', 'Super Racer', 24, 0, 1, 2, 'Trondheim', 'Trondheim', 3, 'Bike made for super speed for men. Does not allow purses.'),
(37, '123', '', 1, 'test', 'test', 12, 0, 1, 0, 'trondheim', 'trondheim', 1, 'test');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bike_discount`
--

CREATE TABLE `bike_discount` (
  `bike_id` int(11) NOT NULL,
  `disc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bike_order`
--

CREATE TABLE `bike_order` (
  `bike_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `bike_order`
--

INSERT INTO `bike_order` (`bike_id`, `order_id`) VALUES
(20, 45),
(21, 45),
(22, 46),
(23, 46),
(32, 47),
(33, 47),
(22, 48),
(24, 48),
(29, 48),
(22, 49),
(24, 49),
(21, 50),
(32, 50),
(25, 51),
(28, 51),
(20, 52),
(23, 52),
(20, 53),
(29, 53);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `bike_type`
--

CREATE TABLE `bike_type` (
  `bike_typeID` int(11) NOT NULL,
  `bike_type` varchar(50) NOT NULL,
  `base_price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `bike_type`
--

INSERT INTO `bike_type` (`bike_typeID`, `bike_type`, `base_price`) VALUES
(1, 'BMX', 400),
(2, 'Offroad', 500),
(3, 'Racer', 600);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cell_num` char(8) NOT NULL,
  `date_ob` date NOT NULL,
  `person_id` int(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `customers`
--

INSERT INTO `customers` (`customer_id`, `name`, `email`, `cell_num`, `date_ob`, `person_id`) VALUES
(73, 'Kristine Løvjomås', 'kristine@gmail.com', '46816740', '1996-01-10', 8372635),
(74, 'Eirik Oscar Horgen Ness', 'eirik.oscar.horgen.ness@gmail.com', '93853500', '1997-09-04', 23495695),
(75, 'Kevin Nordnes', 'kevinnor1997@gmail.com', '41075088', '1997-09-02', 8732367),
(76, 'Matias Skjetne', 'matiaskj@gmail.com', '99121496', '1996-01-30', 9876675),
(77, 'Jon kkkkannon', 'jonkanon@gmail.com', '98347654', '1898-12-03', 347532),
(78, 'John Oscar Ness', 'john.oscar.ness@gmail.com', '40002512', '1968-12-19', 3485683);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `discounts`
--

CREATE TABLE `discounts` (
  `disc_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date NOT NULL,
  `for_type` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `equipment`
--

CREATE TABLE `equipment` (
  `eq_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `amount` int(11) NOT NULL,
  `compatibility` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `equipment`
--

INSERT INTO `equipment` (`eq_id`, `name`, `amount`, `compatibility`) VALUES
(1, 'Helmet', 1000, '-1'),
(2, 'Flask', 1000, '-1'),
(3, 'Gloves', 500, '-1'),
(4, 'Purse', 50, '-1'),
(5, 'Trailer', 20, '-1'),
(6, 'Safety seat', 40, '-1'),
(7, 'Lock', 10, '-1'),
(8, 'Repir kit', 30, '-1');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `equipment_order`
--

CREATE TABLE `equipment_order` (
  `eq_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `equipment_order`
--

INSERT INTO `equipment_order` (`eq_id`, `order_id`, `amount`) VALUES
(1, 48, 1),
(1, 51, 2),
(1, 52, 1),
(1, 53, 23),
(2, 46, 1),
(2, 52, 1),
(3, 45, 2),
(3, 47, 2),
(3, 48, 1),
(3, 49, 1),
(3, 52, 1),
(4, 46, 1),
(4, 49, 1),
(4, 52, 1),
(4, 53, 1);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `locations`
--

CREATE TABLE `locations` (
  `adress` varchar(50) NOT NULL,
  `zipcode` char(4) NOT NULL,
  `zip_location` varchar(50) NOT NULL,
  `info` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `locations`
--

INSERT INTO `locations` (`adress`, `zipcode`, `zip_location`, `info`) VALUES
('Karl Johansgate 1', '0570', 'Oslo', 'Use Bring to transport bikes to Oslo. '),
('Klostergaten 56', '5020', 'Bergen', 'Use PostNord to transport bikes to Bergen. A trailer will arrive every two weeks.'),
('Museumvegen', '6413', 'Molde', 'Use Bring to transport bikes to Molde.'),
('Nordre Fjellbergodden', '3595', 'Haugastøl', 'Call Ole Andersen whenever bikes need to be transported to Haugastøl.'),
('Trondheim', '7051', 'Trondheim', 'Use F-36 to transport bikes to Trondheim ');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `order_comment` varchar(250) DEFAULT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `fulfilled` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `orders`
--

INSERT INTO `orders` (`order_id`, `from_date`, `to_date`, `order_comment`, `customer_id`, `fulfilled`) VALUES
(45, '2019-03-28 00:00:00', '2019-03-31 00:00:00', 'Kristine må slutte å slette databasen! TAKK!', 75, 0),
(46, '2019-04-09 00:00:00', '2019-05-18 00:00:00', 'Hei jeg heter Jon og jeg liker sykkel', 77, 1),
(47, '2019-03-28 00:00:00', '2019-03-30 00:00:00', 'Nå skal jeg på tur og det er ingenting du kan gjøre med det!', 74, 0),
(48, '2019-03-11 00:00:00', '2019-03-17 00:00:00', 'Jenter kan også sykle', 73, 2),
(49, '2019-03-29 00:00:00', '2019-03-30 00:00:00', 'GI MEG SYKKEL!', 74, 0),
(50, '2019-04-30 00:00:00', '2019-05-01 00:00:00', 'Dette er en kommentar', 73, 0),
(51, '2019-04-19 00:00:00', '2019-04-28 00:00:00', 'Jeg vil på tur igjen', 75, 0),
(52, '2019-04-05 00:00:00', '2019-04-06 00:00:00', 'Takk', 74, 0),
(53, '2019-04-10 00:00:00', '2019-04-21 00:00:00', 'this guy needs a lot of helmets', 77, 0);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `order_fulfilled`
--

CREATE TABLE `order_fulfilled` (
  `fulfilled_id` int(11) NOT NULL,
  `fulfilled_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `order_fulfilled`
--

INSERT INTO `order_fulfilled` (`fulfilled_id`, `fulfilled_name`) VALUES
(0, 'Ordered'),
(1, 'Delivered'),
(2, 'Returned'),
(3, 'Deleted');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `repairs`
--

CREATE TABLE `repairs` (
  `rep_id` int(11) NOT NULL,
  `from_date` date NOT NULL,
  `to_date` date DEFAULT NULL,
  `rep_description` varchar(250) DEFAULT NULL,
  `bike_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `repairs`
--

INSERT INTO `repairs` (`rep_id`, `from_date`, `to_date`, `rep_description`, `bike_id`) VALUES
(84, '2019-03-05', '2019-04-05', 'Ramme er skeiv', 22),
(85, '2019-03-28', '2019-04-05', 'Sykkelen er opp ned', 21),
(86, '2019-04-01', '2019-04-05', 'Got a big scracth ', 33),
(87, '2019-04-03', '2019-04-05', 'Fiks an', 24),
(88, '2019-04-04', '2019-04-05', 'Wheel broken', 21),
(89, '2019-04-04', '2019-04-05', 'Skeiv ramme', 32),
(90, '2019-04-04', '2019-04-05', 'den er ødelagt', 23),
(91, '2019-04-04', '2019-04-05', 'ødelagt denne også', 25),
(92, '2019-04-05', '2019-04-05', 'Kevin took a wheel\n', 21),
(93, '2019-04-05', '2019-04-05', 'yes i am very sure', 33),
(94, '2019-04-05', '2019-04-05', 'this is broken; DROP TABLE SYKKEL;', 34),
(95, '2019-04-05', '2019-04-05', '', 35),
(96, '2019-04-05', '2019-04-05', '', 24),
(97, '2019-04-05', '2019-04-05', '', 33),
(98, '2019-04-05', '2019-04-05', '', 35),
(99, '2019-04-05', '2019-04-05', '', 22),
(100, '2019-04-05', NULL, '', 35),
(101, '2019-04-05', NULL, '', 24),
(102, '2018-12-31', NULL, 'Christmas Check', 20),
(103, '2019-04-07', NULL, 'Everything is wrong', 29),
(104, '2019-04-07', NULL, 'Is broken pls fix', 22);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `status`
--

CREATE TABLE `status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `status`
--

INSERT INTO `status` (`status_id`, `status_name`) VALUES
(1, 'Available '),
(2, 'Rented'),
(3, 'Need maintenance'),
(4, 'Scrapped');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `users`
--

CREATE TABLE `users` (
  `userID` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(60) NOT NULL,
  `salt` varchar(20) NOT NULL,
  `adress` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `users`
--

INSERT INTO `users` (`userID`, `username`, `password`, `salt`, `adress`) VALUES
(6, 'oslo', '$2a$10$ZIuYRTCoxNxLiZ4eqzW.8OD0JbdQwKGYyW0Z4cG1mCZFe7iAVSRJa', 'test', 'Karl Johansgate 1'),
(7, 'Bergen', '$2a$10$V/hR3m6LXCJOcdKmXdeAv.CTdFcR9REfJWS4.1/TZ5u3Uyueq67si', 'ta', 'Klostergaten 56'),
(8, 'molde', '$2a$10$ViOs74T25WBV/BhdCEw5juW4X8K80DeY8hexwrjDjpQVhJ0r36cbC', 'ta', 'Museumvegen'),
(9, 'haugastøl', '$2a$10$imsguK3sptss6gikvJPQs.9P8CoTfESmyl6TuY.QQcfV64qK5bl0.', 'ta', 'Nordre Fjellbergodden'),
(10, 'Trondheim', '$2a$10$ilr0sHoyivoNo6x/N3GlueWRo0pJtevE93hyIGp80wzXqCs6uNXba', 'na', 'Trondheim');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `user_age`
--

CREATE TABLE `user_age` (
  `user_age` int(11) NOT NULL,
  `price_modifier` double NOT NULL,
  `age` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `user_age`
--

INSERT INTO `user_age` (`user_age`, `price_modifier`, `age`) VALUES
(0, 0.4, 'Child'),
(1, 1, 'Youth'),
(2, 2, 'Senior');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `user_gender`
--

CREATE TABLE `user_gender` (
  `gender_id` int(11) NOT NULL,
  `gender_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `user_gender`
--

INSERT INTO `user_gender` (`gender_id`, `gender_name`) VALUES
(1, 'Male'),
(2, 'Female'),
(3, 'Uni-sex');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `user_level`
--

CREATE TABLE `user_level` (
  `user_level` int(11) NOT NULL,
  `price_modifier` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `user_level`
--

INSERT INTO `user_level` (`user_level`, `price_modifier`) VALUES
(0, 0.5),
(1, 1),
(2, 0.8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bikes`
--
ALTER TABLE `bikes`
  ADD PRIMARY KEY (`bike_id`),
  ADD KEY `bikes_fk4` (`user_level`),
  ADD KEY `bikes_fk5` (`user_age`),
  ADD KEY `bikes_fk3` (`type`),
  ADD KEY `bikes_fk6` (`status`),
  ADD KEY `bikes_fk7` (`user_gender`),
  ADD KEY `bikes_fk1` (`adress`),
  ADD KEY `bikes_fk2` (`current_adress`);

--
-- Indexes for table `bike_discount`
--
ALTER TABLE `bike_discount`
  ADD PRIMARY KEY (`bike_id`,`disc_id`),
  ADD KEY `bike_discount_fk2` (`disc_id`);

--
-- Indexes for table `bike_order`
--
ALTER TABLE `bike_order`
  ADD PRIMARY KEY (`bike_id`,`order_id`),
  ADD KEY `bike_order_fk2` (`order_id`);

--
-- Indexes for table `bike_type`
--
ALTER TABLE `bike_type`
  ADD PRIMARY KEY (`bike_typeID`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `person_id` (`person_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`disc_id`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`eq_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `equipment_order`
--
ALTER TABLE `equipment_order`
  ADD PRIMARY KEY (`eq_id`,`order_id`),
  ADD KEY `equipment_order_fk2` (`order_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`adress`),
  ADD UNIQUE KEY `adress` (`adress`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `orders_fk1` (`customer_id`),
  ADD KEY `orders_fk2` (`fulfilled`);

--
-- Indexes for table `order_fulfilled`
--
ALTER TABLE `order_fulfilled`
  ADD PRIMARY KEY (`fulfilled_id`);

--
-- Indexes for table `repairs`
--
ALTER TABLE `repairs`
  ADD PRIMARY KEY (`rep_id`),
  ADD KEY `repairs_fk1` (`bike_id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `adress` (`adress`);

--
-- Indexes for table `user_age`
--
ALTER TABLE `user_age`
  ADD PRIMARY KEY (`user_age`),
  ADD UNIQUE KEY `user_age` (`user_age`);

--
-- Indexes for table `user_gender`
--
ALTER TABLE `user_gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `user_level`
--
ALTER TABLE `user_level`
  ADD PRIMARY KEY (`user_level`),
  ADD UNIQUE KEY `user_level` (`user_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bikes`
--
ALTER TABLE `bikes`
  MODIFY `bike_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `bike_type`
--
ALTER TABLE `bike_type`
  MODIFY `bike_typeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `disc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `eq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `repairs`
--
ALTER TABLE `repairs`
  MODIFY `rep_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `user_gender`
--
ALTER TABLE `user_gender`
  MODIFY `gender_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `bikes`
--
ALTER TABLE `bikes`
  ADD CONSTRAINT `bikes_fk1` FOREIGN KEY (`adress`) REFERENCES `locations` (`adress`),
  ADD CONSTRAINT `bikes_fk2` FOREIGN KEY (`current_adress`) REFERENCES `locations` (`adress`),
  ADD CONSTRAINT `bikes_fk3` FOREIGN KEY (`type`) REFERENCES `bike_type` (`bike_typeID`),
  ADD CONSTRAINT `bikes_fk4` FOREIGN KEY (`user_level`) REFERENCES `user_level` (`user_level`),
  ADD CONSTRAINT `bikes_fk5` FOREIGN KEY (`user_age`) REFERENCES `user_age` (`user_age`),
  ADD CONSTRAINT `bikes_fk6` FOREIGN KEY (`status`) REFERENCES `status` (`status_id`),
  ADD CONSTRAINT `bikes_fk7` FOREIGN KEY (`user_gender`) REFERENCES `user_gender` (`gender_id`);

--
-- Begrensninger for tabell `bike_discount`
--
ALTER TABLE `bike_discount`
  ADD CONSTRAINT `bike_discount_fk1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`),
  ADD CONSTRAINT `bike_discount_fk2` FOREIGN KEY (`disc_id`) REFERENCES `discounts` (`disc_id`);

--
-- Begrensninger for tabell `bike_order`
--
ALTER TABLE `bike_order`
  ADD CONSTRAINT `bike_order_fk1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`),
  ADD CONSTRAINT `bike_order_fk2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Begrensninger for tabell `equipment_order`
--
ALTER TABLE `equipment_order`
  ADD CONSTRAINT `equipment_order_fk1` FOREIGN KEY (`eq_id`) REFERENCES `equipment` (`eq_id`),
  ADD CONSTRAINT `equipment_order_fk2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Begrensninger for tabell `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_fk1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  ADD CONSTRAINT `orders_fk2` FOREIGN KEY (`fulfilled`) REFERENCES `order_fulfilled` (`fulfilled_id`);

--
-- Begrensninger for tabell `repairs`
--
ALTER TABLE `repairs`
  ADD CONSTRAINT `repairs_fk1` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`bike_id`);

--
-- Begrensninger for tabell `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `Users_fk1` FOREIGN KEY (`adress`) REFERENCES `locations` (`adress`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
